$(document).ready(function() {

  $('#L2').hide();
  $('#L3').hide();
  $('#L4').hide();

  $('select#cell_phy').on('change', function() {
    $('#L2').hide();
    $('#L3').hide();
    $('#L4').hide();

    switch ($(this).val()) {
        case '2':
            $('#L2').show();
            break;
        case '3':
            $('#L2').show();
            $('#L3').show();
            break;
        case '4':
            $('#L2').show();
            $('#L3').show();
            $('#L4').show();
            break;
    }

  });

  $('select#cell_phy').trigger('change');


  //  plan
  $('#div_pci_group').hide();
  $('#div_pci_sector').hide();

  $('select#plan').on('change', function() {

    switch ($(this).val()) {
        case 'pci':
          $('#div_pci').show();
          $('#div_pci_group').hide();
          $('#div_pci_sector').hide();
            break;
        case 'pci_group':
          $('#div_pci').hide();
          $('#div_pci_group').show();
          $('#div_pci_sector').hide();
            break;
        case 'pci_sector':
          $('#div_pci').hide();
          $('#div_pci_group').hide();
          $('#div_pci_sector').show();
            break;
    }

  });

  $('select#plan').trigger('change');


var response_rsi = [];

  $("form#form_config_site").submit(function (e) {
    e.preventDefault();
    $.ajax({
      type: 'post',
      url: '/planning_lte/config_site',
     // dataType: 'json',
      //async: true,
      data: $(this).serialize(),
      success: function (response) {
        //$('#commentaire-modal').modal();
        //console.log(response);

         $("#site_name_config").val(response.site_name);

          response_rsi = response.rsi;

          //console.log(response.u);

          var pci_group_option = '<option value="">Select PCI Group</option>';
          $.each(response.pci_group, function(i,data){
             pci_group_option+='<option value="'+data+'">'+data+'</option>';
         });

          $("select#pci_group_1").html(pci_group_option);
          $("select#pci_group_2").html(pci_group_option);


          var u_option = '';
          $.each(response.u, function(i,data){
             u_option+='<option value="'+data+'">'+data+'</option>';
         });

          $("select#u_config_s1").html(u_option);
          $("select#u_config_s2").html(u_option);
          $("select#u_config_s3").html(u_option);
          $("select#u_config_s4").html(u_option);



          $("#block_suggestion").slideDown();
        
      },
      error: function (response) {
        console.log("err" + response);
      }
    });
  });



  $('select#pci_group_1').on('change', function() {

    var $pci_group_1 = $(this).val() * 3;

    option1 = '';
    option1+='<option value="'+($pci_group_1 + 0)+'">'+($pci_group_1 + 0)+'</option>';
    option1+='<option value="'+($pci_group_1 + 1)+'">'+($pci_group_1 + 1)+'</option>';
    option1+='<option value="'+($pci_group_1 + 2)+'">'+($pci_group_1 + 2)+'</option>';

    $("select#pci_config_s1").html(option1);
    $("select#pci_config_s2").html(option1);
    $("select#pci_config_s3").html(option1);


    var $pci_group_2 = $('select#pci_group_2').val() * 3;

    option2 = '';
    option2+='<option value="'+($pci_group_2 + 0)+'">'+($pci_group_2 + 0)+'</option>';
    option2+='<option value="'+($pci_group_2 + 1)+'">'+($pci_group_2 + 1)+'</option>';
    option2+='<option value="'+($pci_group_2 + 2)+'">'+($pci_group_2 + 2)+'</option>';

    $("select#pci_config_s4").html(option2);


    $("#block_config").slideDown();


  });



  $('select#pci_group_2').on('change', function(e) {
    e.preventDefault();

    var $pci_group_2 = $(this).val() * 3;

    option2 = '';
    option2+='<option value="'+($pci_group_2 + 0)+'">'+($pci_group_2 + 0)+'</option>';
    option2+='<option value="'+($pci_group_2 + 1)+'">'+($pci_group_2 + 1)+'</option>';
    option2+='<option value="'+($pci_group_2 + 2)+'">'+($pci_group_2 + 2)+'</option>';

    $("select#pci_config_s4").html(option2);


    var $pci_group_1 = $('select#pci_group_1').val() * 3;

    option1 = '';
    option1+='<option value="'+($pci_group_1 + 0)+'">'+($pci_group_1 + 0)+'</option>';
    option1+='<option value="'+($pci_group_1 + 1)+'">'+($pci_group_1 + 1)+'</option>';
    option1+='<option value="'+($pci_group_1 + 2)+'">'+($pci_group_1 + 2)+'</option>';

    $("select#pci_config_s1").html(option1);
    $("select#pci_config_s2").html(option1);
    $("select#pci_config_s3").html(option1);


    $("#block_config").slideDown();

  });


  $('select#prachcs').on('change', function(e) {
    e.preventDefault();

    var $prachcs = $(this).val();

    console.log($prachcs);
    if($prachcs == 12){



          var option_prachcs = '';
          $.each(response_rsi, function(i,data){
             option_prachcs+='<option value="'+data+'">'+data+'</option>';
         });


        $("select#rsi_config_s1").html(option_prachcs);
        $("select#rsi_config_s2").html(option_prachcs);
        $("select#rsi_config_s3").html(option_prachcs);
        $("select#rsi_config_s4").html(option_prachcs);

        $("#block_config").slideDown();
       
    } else {

        $("select#rsi_config_s1").html('');
        $("select#rsi_config_s2").html('');
        $("select#rsi_config_s3").html('');
        $("select#rsi_config_s4").html('');
    }

  });


  
    $("#delete_site").click(function (e) {
        e.preventDefault();
        
          bootbox.confirm("Are you sure?", function(result) {

            if(result){
              var select_val = $("select#sitename_config").val();
              var site_name = select_val.split(',')[3];

              $.ajax({
                type: 'post',
                url: '/planning_lte/delete_site',
                data: { site_name: site_name },
                success: function (response) {
                  $("select#sitename_config option[value='"+select_val+"']").remove();
                   toastr.success('Site Deleted Success', "Delete Site");
                },
                error: function (response) {
                  console.log("err" + response);
                   toastr.error('Error Deleting Site', "Delete Site");
                }
              });
            }
          }); 
      });



  $('select#map_cell_code').on('change', function(e) {
    e.preventDefault();

      var select_val = $(this).val();

      $.ajax({
        type: 'post',
        url: '/administration/get_cell_code',
        data: { Cell_code: select_val },
        success: function (response) {

          $("#X").val(response.X);
          $("#Y").val(response.Y);
          $("#AZ").val(response.AZ);
          $("#PCI").val(response.PCI);
          $("#RSI").val(response.RSI);
          $("#grp_AssigPU").val(response.grp_AssigPU);

           toastr.success('Cellule Get Data Success', "Update Cellule");
        },
        error: function (response) {
          console.log("err" + response);
           toastr.error('Error Cellule Get Data', "Update Cellule");
        }
      });
 });




$("form#form_update_cell").submit(function (e) {
    e.preventDefault();


      $.ajax({
        type: 'post',
        url: '/administration/update_cell_code',
        data: $(this).serialize(),
        success: function (response) {

          console.log(response);
          /*

          $("#X").val(response.X);
          $("#Y").val(response.Y);
          $("#AZ").val(response.AZ);
          $("#PCI").val(response.PCI);
          $("#RSI").val(response.RSI);
          $("#grp_AssigPU").val(response.grp_AssigPU);
          */

           toastr.success('Cellule Update Data Success', "Update Cellule");
        },
        error: function (response) {
          console.log("err" + response);
           toastr.error('Error Cellule Update Data', "Update Cellule");
        }
      });
 });






});
