<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends SX_Model {
	/*
	Contoh Aplikasi Web/Weblog/Website Sederhana Codeigniters
	Oleh Ahmad Rizal Afani
	http://calonpresident.blogspot.coms
	file : blog_model.php untuk load data dari database
	*/
	function GetMarker($band = false, $distinct = false){
		//return $this->db->query("select * from map $where;");

		if($distinct == true){
			$this->db->distinct();
			//$this->db->group_by('site_name');
			$this->db->select('site_name,VENDOR,TAC,Zone,Statut,X,Y');
		}

		//$this->db->select('*');
		if($band != false){
			$this->db->where('BAND_4G', $band);
		}
		$query = $this->db->get('map');

		//var_dump($query->num_rows());
		//exit();

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function GetAll(){

		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function getCellCode($Cell_code){

		$this->db->where('Cell_code', $Cell_code);

		$this->db->limit(1);

		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
			return $query->result()[0];
		}
	}




	function GetRSI(){

		$this->db->distinct();
		$this->db->select('RSI')->order_by('RSI');

		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}


	function GetU(){

		$this->db->distinct();
		$this->db->select('(PCI + grp_AssigPU) as u')->order_by('u');

		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	function GetStat(){

			//$this->db->distinct();
			//$this->db->group_by('site_name');
			//$this->db->select('site_name,VENDOR,TAC,Zone,Statut,X,Y')->order_by('X');
			$this->db->select('*')->order_by('site_name');


		$query = $this->db->get('map');
		//var_dump($query->num_rows());
		//var_dump($query->result());
		//exit();


		if ($query->num_rows() > 0)
		{
			$site_name = '';
			$BAND_800 = false;
			$BAND_1800 = false;

			$BAND_4G_dual = 0;
			$BAND_4G_800 = 0;
			$BAND_4G_1800 = 0;
			$BAND_4G_total = 0;

			$site_name_dual = array();
			$site_name_800 = array();
			$site_name_1800 = array();


			foreach ($query->result() as $row){


					if($site_name!='' and $row->site_name != $site_name){

						/*
						var_dump($site_name);
						var_dump($row->site_name);
						var_dump($BAND_800);
						var_dump($BAND_1800);
						print '<hr>';
						*/

						if($BAND_800 == true and $BAND_1800 == true){
							$BAND_4G_dual++;
							$site_name_dual[] = $site_name;
						}

						if($BAND_800 == true and $BAND_1800 == false){
							$BAND_4G_800++;
							$site_name_800[] = $site_name;
						}

						if($BAND_800 == false and $BAND_1800 == true){
							$BAND_4G_1800++;
							$site_name_1800[] = $site_name;
						}

						$BAND_4G_total++;

						$BAND_800 = false;
						$BAND_1800 = false;
					}

				$site_name = $row->site_name;

				if($row->BAND_4G == 800){
					$BAND_800 = true;
				}

				if($row->BAND_4G == 1800){
					$BAND_1800 = true;
				}

			}


			if($BAND_800 == true and $BAND_1800 == true){
				$BAND_4G_dual++;
				$site_name_dual[] = $site_name;
			}

			if($BAND_800 == true and $BAND_1800 == false){
				$BAND_4G_800++;
				$site_name_800[] = $site_name;
			}

			if($BAND_800 == false and $BAND_1800 == true){
				$BAND_4G_1800++;
				$site_name_1800[] = $site_name;
			}


			//var_dump($BAND_4G_dual);
		//	var_dump($BAND_4G_800);
			//var_dump($BAND_4G_1800);
/*
			var_dump($site_name_dual);
		  var_dump($site_name_800);
			var_dump($site_name_1800);

			exit();
			*/
			return array('BAND_4G_dual' => $BAND_4G_dual, 'BAND_4G_800' => $BAND_4G_800, 'BAND_4G_1800' => $BAND_4G_1800,'BAND_4G_total' => $BAND_4G_total);
		}
	}


	function GetStatPCI(){

		$this->db->select('*')->order_by('PCI');
		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
		$pci = '';

		$BAND_4G_800 = 0;
		$BAND_4G_1800 = 0;

		$pci_array = array();

		foreach ($query->result() as $row){

			if($pci!='' and $row->PCI != $pci){
				$pci_array[] = array('PCI' => $pci, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);
				$BAND_4G_800 = 0;
				$BAND_4G_1800 = 0;
			}

			$pci = $row->PCI;

			if($row->BAND_4G == 800){
				$BAND_4G_800++;
			}

			if($row->BAND_4G == 1800){
				$BAND_4G_1800++;
			}

		}


		$pci_array[] = array('PCI' => $pci, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);

		return $pci_array;
		}
	}



	function GetStatRSI(){

		$this->db->select('*')->order_by('RSI');
		$query = $this->db->get('map');

		if ($query->num_rows() > 0)
		{
			$rsi = '';

			$BAND_4G_800 = 0;
			$BAND_4G_1800 = 0;

			$rsi_array = array();

			foreach ($query->result() as $row){

				if($rsi!='' and $rsi != $row->RSI ){
					$rsi_array[] = array('RSI' => $rsi, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);
					$BAND_4G_800 = 0;
					$BAND_4G_1800 = 0;
				}

				$rsi = $row->RSI;

				if($row->BAND_4G == 800){
					$BAND_4G_800++;
				}

				if($row->BAND_4G == 1800){
					$BAND_4G_1800++;
				}

			}


			$rsi_array[] = array('RSI' => $rsi, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);

			return $rsi_array;
		}
	}



function GetStatRSI2(){

	$this->db->select('RSI')->order_by('RSI');

	$query = $this->db->get('map');

	if ($query->num_rows() > 0){

		$rsi = '';
		$nb_rsi = 0;
		$rsi_array = array();

		foreach ($query->result() as $row){

			if($rsi!='' and $row->RSI != $rsi){
				$rsi_array[] = array('RSI' => $rsi, 'val' => $nb_rsi);
				$nb_rsi = 0;
			}

			$rsi = $row->RSI;
			$nb_rsi++;

		}

		$rsi_array[] = array('RSI' => $rsi, 'val' => $nb_rsi);

		return $rsi_array;
	}
}



	function GetStatU(){

		$this->db->select('BAND_4G, MOD((PCI + grp_AssigPU), 30) as u')->order_by('u');
		$query = $this->db->get('map');

		//var_dump($this->db->last_query());
		//exit();

		if ($query->num_rows() > 0)
		{
			$u = '';

			$BAND_4G_800 = 0;
			$BAND_4G_1800 = 0;

			$u_array = array();

			foreach ($query->result() as $row){

				if($u!='' and $u != $row->u ){
					$u_array[] = array('U' => $u, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);
					$BAND_4G_800 = 0;
					$BAND_4G_1800 = 0;
				}

				$u = $row->u;

				if($row->BAND_4G == 800){
					$BAND_4G_800++;
				}

				if($row->BAND_4G == 1800){
					$BAND_4G_1800++;
				}

			}


			$u_array[] = array('U' => $u, '800' => $BAND_4G_800, '1800' => $BAND_4G_1800);

			return $u_array;
		}
	}

function GetStatU2(){

	$this->db->select('*')->order_by('PCI')->order_by('grp_AssigPU');
	$query = $this->db->get('map');

	if ($query->num_rows() > 0){

		$u = '';
		$nb_u = 0;
		$u_array = array();

		foreach ($query->result() as $row){

			if($u!='' and (( intval($row->PCI) + intval($row->grp_AssigPU) ) % 30) != $u){
				$u_array[] = array('U' => $u, 'val' => $nb_u);
				$nb_u = 0;
			}

			$u = ( intval($row->PCI) + intval($row->grp_AssigPU) ) % 30;
			$nb_u++;

		}

		$u_array[] = array('U' => $u, 'val' => $nb_u);

		//print '<pre>';
		//print_r($u_array);
		//print '</pre>';
		//exit();
		$u_array_fin = array();
		$array_filter = array();

		foreach ($u_array as $row){
			$nb_us = 0;

			foreach ($u_array as $row_d){
				if($row_d['U']==$row['U']){
					$nb_us+= $row_d['val'];
				}
			}
			if (!in_array($row['U'], $array_filter)) {
				$u_array_fin[] = array('U' => $row['U'], 'val' => $nb_us);
			}

			$array_filter[] = $row['U'];

		}

		$array_return = array();
		for($i=0;$i<40;$i++){
			foreach ($u_array_fin as $row){
				if($row['U']== $i){
					$array_return[] = array('U' => $row['U'], 'val' => $row['val']);
				}
			}
		}

		return $array_return;
	}
}





	public function InsertSite($data){

		$data = (object)$data;

		foreach($data->BAND_4G  as  $BAND_4G){

			for($i=1;$i<((int)$data->cell_phy)+1;$i++){
				$line = array(
					'site_name' => $data->site_name,
					'cell_phy' => $data->site_name.'_'.$i,
					'Cell_code' => 'MARWA',
					'VENDOR' => 'NSN',
					'PCI' => $data->{'L'.$i.'_PCI'},
					'RSI' => $data->{'L'.$i.'_RSI'},
					'eNodeB_ID' => '0',
					'grp_AssigPU' => $data->{'L'.$i.'_GrpA'},
					'TAC' => '4164',
					'BAND_4G' => $BAND_4G,
					'SITE_LOG' => 'MARWA',
					'X' => $data->X,
					'Y' => $data->Y,
					'AZ' => $data->{'L'.$i.'_AZ'},
					'UARFCN' => '6400',
					'Zone' => 'BAR',
					'Statut' => 'onair'
				);

				$this->db->insert('map', $line);
			}
		}

		return true;
	}


	public function UpdateCell($data){

		$data = (object)$data;

		$line  = array(
		  'X' 	 		=> $data->X,
		  'Y'	 		=> $data->Y,
		  'AZ'			=> $data->AZ,
		  'PCI'	 		=> $data->PCI,
		  'RSI'	 		=> $data->RSI,
		  'grp_AssigPU'	=> $data->grp_AssigPU
		);

		return $this->db->update('map', $line, 
			array('Cell_code' => $data->Cell_code)
		);			
	}



	public function UpdateSite($data){

		$data = (object)$data;

		if($data->pci_config_s1 != ''){
			$grp_AssigPU1 = ((((intdiv($data->pci_config_s1, 30)+1)*30)+$data->u_config_s1-$data->pci_config_s1) % 30);
			$line1 = array(
				'PCI' => $data->pci_config_s1,
				'RSI' => $data->rsi_config_s1,
				'grp_AssigPU' => $grp_AssigPU1
			);

			$this->db->update('map', $line1, 
				array('site_name' => $data->site_name_config,'cell_phy' => $data->site_name_config.'_1')
			);			
		}


		if($data->pci_config_s2 != ''){
			$grp_AssigPU2 = ((((intdiv($data->pci_config_s2, 30)+1)*30)+$data->u_config_s2-$data->pci_config_s2) % 30);
			$line2 = array(
				'PCI' => $data->pci_config_s2,
				'RSI' => $data->rsi_config_s2,
				'grp_AssigPU' => $grp_AssigPU2
			);

			$this->db->update('map', $line2, 
				array('site_name' => $data->site_name_config,'cell_phy' => $data->site_name_config.'_2')
			);	
		}


		if($data->pci_config_s3 != ''){
			$grp_AssigPU3 = ((((intdiv($data->pci_config_s3, 30)+1)*30)+$data->u_config_s3-$data->pci_config_s3) % 30);
			$line3 = array(
				'PCI' => $data->pci_config_s3,
				'RSI' => $data->rsi_config_s3,
				'grp_AssigPU' => $grp_AssigPU3
			);

			$this->db->update('map', $line3, 
				array('site_name' => $data->site_name_config,'cell_phy' => $data->site_name_config.'_3')
			);			
		}


		if($data->pci_config_s4 != ''){
			$grp_AssigPU4 = ((((intdiv($data->pci_config_s4, 30)+1)*30)+$data->u_config_s4-$data->pci_config_s4) % 30);
			$line4 = array(
				'PCI' => $data->pci_config_s4,
				'RSI' => $data->rsi_config_s4,
				'grp_AssigPU' => $grp_AssigPU4
			);

			$this->db->update('map', $line4, 
				array('site_name' => $data->site_name_config,'cell_phy' => $data->site_name_config.'_4')
			);		

			//var_dump($this->db->last_query());		
		}
						
		return true;
	}


	function DeleteSite($data){
		return $this->db->delete('map',array('site_name' => $data));
		//var_dump($this->db->last_query());	
	}


	function GetPoints($data = ''){


		//r = d/R = (1000 km)/(6371 km) = 0.1570
		$r = round($data['diastance']/6371, 4,PHP_ROUND_HALF_UP);

		$latmin = round(deg2rad($data['lat']) - $r, 4,PHP_ROUND_HALF_UP); 
		$latmax = round(deg2rad($data['lat']) + $r, 4,PHP_ROUND_HALF_UP);

		$longmin = round(deg2rad($data['long']) - $r, 4,PHP_ROUND_HALF_UP);
		$longmax = round(deg2rad($data['long']) + $r, 4,PHP_ROUND_HALF_UP);


/*
	return 'SELECT * FROM map WHERE (radians(X) >= '.$latmin.' AND radians(X) <= '.$latmax.') AND (radians(Y) >= '.$longmin.' AND radians(Y) <= '.$longmax.')	AND acos(sin(1.3963) * sin(radians(X)) + cos(1.3963) * cos(radians(X)) * cos(radians(Y) - (-0.6981))) <= '.$r;
	*/
/*

SELECT * FROM Places WHERE
    (Lat >= 1.2393 AND Lat <= 1.5532) AND (Lon >= -1.8184 AND Lon <= 0.4221)
HAVING
    acos(sin(1.3963) * sin(Lat) + cos(1.3963) * cos(Lat) * cos(Lon - (-0.6981))) <= 0.1570;


	SELECT * FROM map WHERE
	    (radians(X) >= $latmin AND radians(X) <= $latmax) AND (radians(Y) >= $longmin AND radians(Y) <= $longmax)
	AND
	    acos(sin(1.3963) * sin(radians(X)) + cos(1.3963) * cos(radians(X)) * cos(radians(Y) - (-0.6981))) <= $r;
*/
		
		$this->db->select('*');
		$this->db->where('(radians(X) >= '.$latmin.' AND radians(X) <= '.$latmax.') AND (radians(Y) >= '.$longmin.' AND radians(Y) <= '.$longmax.')');
		/*
		$this->db->where('(radians(X) >= '.$latmin.' AND radians(X) <= '.$latmax.') AND (radians(Y) >= '.$longmin.' AND radians(Y) <= '.$longmax.')	AND acos(sin(1.3963) * sin(radians(X)) + cos(1.3963) * cos(radians(X)) * cos(radians(Y) - (-0.6981))) <= '.$r);
*/
		$query = $this->db->get('map');

		//var_dump($data);
		//var_dump($this->db->last_query());

		if ($query->num_rows() > 0)
		{
			return $query->result();
		}

	}


	public function Add_User($data_user){
		$this->db->insert('user', $data_user);
	}

	public function upload($config)
    {
        if(!file_exists($config['upload_path']))
        {
            mkdir($config['upload_path'], 0777, true);
        }
        
        if($this->upload->do_upload())
        {
            return true;
        }
        else
        {
            return false;
        }    
    } 

     function get_addressbook() {     
        $query = $this->db->get('addressbook');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_csv($data) {
        $this->db->insert('map', $data);
    }

	function GetQuery($nama_tabel){
		return $this->db->get($nama_tabel);
	}


	function GetLabel($where = ''){
		return $this->db->query("select * from label $where;");
	}

	function GetLabelContent($where = ''){
		return $this->db->query("select * from content_label $where;");
	}

	function GetUser($where = ''){
		return $this->db->query("select * from userapp $where;");
	}

	function GetContent($where = ''){
		return $this->db->query("select * from content $where;");
	}

	function GetContentJoinLabel($where = ''){
		return $this->db->query("select * from content inner join content_label on content.kode_content=content_label.kode_content $where;");
	}

	function GetContentView(){
		return $this->db->query("select sum(counter) as totalview from content where status = 'publish'");
	}

	function GetSetting(){
		return $this->db->query("select * from setting;");
	}

	function GetComment($where = ""){
		return $this->db->query("select content.judul_content,komentar.*  from content inner join komentar on komentar.kode_content=content.kode_content $where;");
	}

	/* Queries for blog */
	function GetContentBlog($where = ""){
		return $this->db->query("select content.*,count(komentar.kode_comment)as totalkomentar from content left join (select * from komentar where status = 'publish' group by kode_content)as komentar on content.kode_content=komentar.kode_content inner join content_label on content.kode_content=content_label.kode_content $where;");
	}

	function GetContentPublished($where = ""){
		return $this->db->query("select count(*)as total from (select content.*,count(komentar.kode_comment)as totalkomentar from content left join komentar on content.kode_content=komentar.kode_content inner join content_label on content.kode_content=content_label.kode_content $where group by content.kode_content order by content.kode_content desc) as temp");
	}

	function GetContentDetail($where = ""){
		return $this->db->query("select content.*,count(komentar.kode_comment)as totalkomentar from content inner join komentar on content.kode_content=komentar.kode_content $where;");
	}

	function GetVisitor($where = ""){
		return $this->db->query("select * from visitor $where");
	}

	public function InsertData($table,$data){
		return $this->db->insert($table,$data);
	}

	public function UpdateData($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}

	public function DeleteData($table,$where){
		return $this->db->delete($table,$where);
	}
}
?>
