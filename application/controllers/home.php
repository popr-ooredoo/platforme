<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends SX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

public function __construct()
 {
	 parent::__construct();

	 $this->load->model('Home_model');
	 $this->load->library(array('ion_auth','form_validation'));

 }

	public function index()
	{
			if (!$this->ion_auth->logged_in())
			{
				// redirect them to the login page
				redirect('auth/login', 'refresh');
			}

		// Set the title
        $this->template->title = 'Welcome marwa!';
        $this->template->pagename = 'home';



			// Dynamically add a css stylesheet
			$this->template->stylesheet->add('assets/plugins/morris/morris.css');

			$this->template->javascript->add('assets/plugins/morris/morris.min.js');
			$this->template->javascript->add('assets/plugins/morris/raphael-min.js');

			//$this->template->javascript->add('assets/scripts/charts-morris.js');


			$this->data['distribution'] = $this->Home_model->GetStat();
			$this->data['distribution_pci'] = $this->Home_model->GetStatPCI();
			$this->data['distribution_rsi'] = $this->Home_model->GetStatRSI();
			$this->data['distribution_u'] = $this->Home_model->GetStatU();

//print_r($this->data['distribution_u']);
//exit();
			


			//var_dump($this->data['distribution']);
			//exit();






			//$this->data = array('title' => 'Hello, world!'); // load from model (but using a dummy array here)
			$this->template->content->view('home/index', $this->data);
			// Publish the template
			$this->template->publish();

	}


	public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}
