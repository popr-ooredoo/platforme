<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planning_LTE extends SX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
	  {
	 	 parent::__construct();

	 	 $this->load->model('Home_model');
		 $this->load->helper('form');
	 	 $this->load->library(array('ion_auth','form_validation'));


	  }

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		// Set the title
        $this->template->title = 'Planning LTE';
        $this->template->pagename = 'planning_lte';

			// Dynamically add a css stylesheet
			$this->template->stylesheet->add('assets/plugins/select2/css/select2.min.css');
			$this->template->stylesheet->add('assets/plugins/select2/css/select2-bootstrap.min.css');
			$this->template->javascript->add('assets/plugins/select2/js/select2.full.min.js');
			$this->template->javascript->add('assets/scripts/components-select2.js');

			$this->template->stylesheet->add('assets/css/components.css');
			$this->template->stylesheet->add('assets/css/plugins.min.css');

			$this->template->javascript->add('assets/plugins/bootbox/bootbox.min.js');

			$this->template->javascript->add('assets/plugins/jquery-validation/js/jquery.validate.min.js');
			$this->template->javascript->add('assets/plugins/jquery-validation/js/additional-methods.min.js');
			$this->template->javascript->add('assets/scripts/form-validation.js');

			//$this->template->javascript->add('http://www.openlayers.org/api/OpenLayers.js');
			$this->template->javascript->add('assets/scripts/map.js');

			$this->data['maps'] = $this->Home_model->GetMarker(false,true);
			$this->data['maps_800'] = $this->Home_model->GetMarker('800');
			$this->data['maps_1800'] = $this->Home_model->GetMarker('1800');


			//var_dump($this->input->get());
			//var_dump($this->input->post());
			$this->data['pci'] = $this->input->get('pci');
			$this->data['pci_group'] = $this->input->get('pci_group');
			$this->data['pci_sector'] = $this->input->get('pci_sector');

			$this->data['rsi'] = $this->input->get('rsi');
			$this->data['u'] = $this->input->get('u');


			if ($this->input->post('form_name') == 'form_add_site') {

				if ($this->form_validation->run('form_add_site')) {
					$add_site  = array(
						'site_name'	=> $this->input->post('site_name'),
						'X'			=> $this->input->post('X'),
						'Y'			=> $this->input->post('Y'),
						'BAND_4G' 	=> $this->input->post('BAND_4G'),
						'cell_phy' 	=> $this->input->post('cell_phy'),
						'L1_AZ'		=> $this->input->post('L1_AZ'),
						'L1_PCI' 	=> $this->input->post('L1_PCI'),
						'L1_RSI' 	=> $this->input->post('L1_RSI'),
						'L1_GrpA' 	=> $this->input->post('L1_GrpA'),
						'L2_AZ' 	=> $this->input->post('L2_AZ'),
						'L2_PCI' 	=> $this->input->post('L2_PCI'),
						'L2_RSI' 	=> $this->input->post('L2_RSI'),
						'L2_GrpA' 	=> $this->input->post('L2_GrpA'),
						'L3_AZ' 	=> $this->input->post('L3_AZ'),
						'L3_PCI' 	=> $this->input->post('L3_PCI'),
						'L3_RSI' 	=> $this->input->post('L3_RSI'),
						'L3_GrpA' 	=> $this->input->post('L3_GrpA'),
						'L4_AZ' 	=> $this->input->post('L4_AZ'),
						'L4_PCI' 	=> $this->input->post('L4_PCI'),
						'L4_RSI' 	=> $this->input->post('L4_RSI'),
						'L4_GrpA' 	=> $this->input->post('L4_GrpA')

					);

					$this->Home_model->InsertSite($add_site);
					$this->session->set_flashdata('success_add_site', 'Data added Success');
					redirect('/planning_lte/index', 'refresh');
				} else {
					$this->data['form_add_site_errors'] = validation_errors('<div class="alert alert-danger"><button class="close" data-close="alert"></button>', '</div>');
					
					$this->session->set_flashdata('error_add_site', validation_errors());
					
					$this->template->content->view('planning_lte/index', $this->data);
					// Publish the template
					$this->template->publish();
				}

			} elseif ($this->input->post('form_name') == 'form_update_site') {

				if ($this->form_validation->run('form_update_site')) {

					$update_site  = array(
					  'site_name_config' => $this->input->post('site_name_config'),
					  'pci_config_s1' 	 => $this->input->post('pci_config_s1'),
					  'rsi_config_s1'	 => $this->input->post('rsi_config_s1'),
					  'u_config_s1'		 => $this->input->post('u_config_s1'),
					  'pci_config_s2'	 => $this->input->post('pci_config_s2'),
					  'rsi_config_s2'	 => $this->input->post('rsi_config_s2'),
					  'u_config_s2'		 => $this->input->post('u_config_s2'),
					  'pci_config_s3'	 => $this->input->post('pci_config_s3'),
					  'rsi_config_s3'	 => $this->input->post('rsi_config_s3'),
					  'u_config_s3'		 => $this->input->post('u_config_s3'),
					  'pci_config_s4'	 => $this->input->post('pci_config_s4'),
					  'rsi_config_s4'	 => $this->input->post('rsi_config_s4'),
					  'u_config_s4'		 => $this->input->post('u_config_s4')
					);

					$this->Home_model->UpdateSite($update_site);
					$this->session->set_flashdata('success_update_site', 'Data Updated Success');
					redirect('/planning_lte/index', 'refresh');
				} else {
					$this->data['form_update_site_errors'] = validation_errors('<div class="alert alert-danger"><button class="close" data-close="alert"></button>', '</div>');

					$this->session->set_flashdata('error_update_site', validation_errors());

					$this->template->content->view('planning_lte/index', $this->data);
					// Publish the template
					$this->template->publish();
				}

			} else {
				$this->template->content->view('planning_lte/index', $this->data);
				// Publish the template
				$this->template->publish();
			}
	}

	public function config_site()
	{

		$latlong =  explode(",", $this->input->post('sitename_config')); 

		$data = array(
			"lat" => $latlong[0],
			"long" => $latlong[1],
			"site_name" => $latlong[3],
			"diastance" => $this->input->post('diastance_config') / 1000
		);

//			"config" => $this->input->post('config')

			$all_points_rsi = $this->Home_model->GetRSI();
			$all_points_u = $this->Home_model->GetU();			
			$points = $this->Home_model->GetPoints($data);

			//var_dump($all_points_u);
			//print_r($all_points_u);
			//exit();

			$pci_groups = array();
			$exist_pci_group = array();
			$select_pci_group = array();

			$all_rsi = array();
			$exist_rsi = array();
			$select_rsi = array();


			$all_u = array();
			$exist_u = array();
			$select_u = array();


			for($i=0;$i<168;$i++){
				$pci_groups[] = $i;
			}

			foreach ($all_points_rsi as $point){
				$all_rsi[] = $point->RSI;
			}

			foreach ($all_points_u as $point){
				$var_all_u = ($point->u % 30);
				if(!in_array($var_all_u, $all_u)){
					$all_u[] = $var_all_u;
				}
			}


			foreach ($points as $point){
				$var_pci_group = intdiv($point->PCI, 3);
				if(!in_array($var_pci_group, $exist_pci_group)){
					$exist_pci_group[] = $var_pci_group;
				}

				if(!in_array($point->RSI, $exist_rsi)){
					$exist_rsi[] = $point->RSI;
				}

				$var_u = (($point->PCI + $point->grp_AssigPU) % 30);
				if(!in_array($var_u, $exist_u)){
					$exist_u[] = $var_u;
				}
			}

			foreach ($pci_groups as $pci_group){
				if(!in_array($pci_group, $exist_pci_group)){
					$select_pci_group[] = $pci_group;
				}
			}

			foreach ($all_rsi as $rsi){
				if(!in_array($rsi, $exist_rsi)){
					$select_rsi[] = $rsi;
				}
			}


			foreach ($all_u as $u){
				if(!in_array($u, $exist_u)){
					$select_u[] = $u;
				}
			}



			//print_r($all_u);
			//print_r($exist_u);
			//print_r($select_u);
			//exit();



	    	$response = array(
	    		'site_name' => $data['site_name'],
	    		'pci_group' => $select_pci_group,
	    		'rsi' 		=> $select_rsi,
	    		'u' 		=> $select_u	    		
	    	);





		header("Content-type: text/json");
		header("Content-type: application/json");
		echo json_encode($response,true);



/*

		var_dump($r);
		var_dump($latmin);
		var_dump($latmax);
		var_dump($longmin);
		var_dump($longmax);
*/

		//echo json_encode($data,true);
		/*
	
/var/www/platforme.dev/application/controllers/Planning_lte.php:145:float 0.00047088369172814
/var/www/platforme.dev/application/controllers/Planning_lte.php:146:float 0.17785813263079
/var/www/platforme.dev/application/controllers/Planning_lte.php:147:float 0.17879990001425
/var/www/platforme.dev/application/controllers/Planning_lte.php:148:float 0.6403808563848
/var/www/platforme.dev/application/controllers/Planning_lte.php:149:float 0.64132262376826

/var/www/platforme.dev/application/controllers/Planning_lte.php:145:float 0.0005
/var/www/platforme.dev/application/controllers/Planning_lte.php:146:float 0.179
/var/www/platforme.dev/application/controllers/Planning_lte.php:147:float 0.18
/var/www/platforme.dev/application/controllers/Planning_lte.php:148:float 0.6407
/var/www/platforme.dev/application/controllers/Planning_lte.php:149:float 0.6417

/var/www/platforme.dev/application/controllers/Planning_lte.php:144:float 0.0005
/var/www/platforme.dev/application/controllers/Planning_lte.php:145:float 0.1776
/var/www/platforme.dev/application/controllers/Planning_lte.php:146:float 0.1786
/var/www/platforme.dev/application/controllers/Planning_lte.php:147:float 0.6412
/var/www/platforme.dev/application/controllers/Planning_lte.php:148:float 0.6422


	SELECT * FROM map WHERE
	    (radians(X) => 0.1776 AND radians(X) <= 0.1786) AND (radians(Y) >= 0.6412 AND radians(Y) <= 0.6422)
	AND
	    acos(sin(1.3963) * sin(radians(X)) + cos(1.3963) * cos(radians(X)) * cos(radians(Y) - (-0.6981))) <= 0.0005;



*/


	}

	public function delete_site(){
		echo $this->Home_model->DeleteSite($this->input->post('site_name'));
	}




}
