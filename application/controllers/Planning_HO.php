<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planning_HO extends SX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
	  {
	 	 parent::__construct();

	 	 $this->load->model('Home_model');
	 	 $this->load->library(array('ion_auth','form_validation'));


	  }


	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		// Set the title
        $this->template->title = 'Planning HO';
        $this->template->pagename = 'planning_ho';

			// Dynamically add a css stylesheet
			//$this->template->stylesheet->add('https://openlayers.org/en/v4.0.1/css/ol.css');
			//$this->template->javascript->add('assets/scripts/map.js');


			$this->template->content->view('planning_ho/index', $this->data);


			// Publish the template
			$this->template->publish();

	}

}
