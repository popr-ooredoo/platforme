<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends SX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
	  {
	 	 parent::__construct();

	 	 $this->load->model('Home_model');
	 	 $this->load->library(array('ion_auth','form_validation'));

		 $this->load->database();
		 $this->load->helper(array('url','language'));

		 $this->lang->load('auth');


		$this->load->library('excel');//load PHPExcel library 
		//$this->load->model('upload_model');//To Upload file in a directory

		 $this->load->library('csvimport');

	  }

	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		// Set the title
        $this->template->title = 'Administartion';
        $this->template->pagename = 'admin';

				$this->template->stylesheet->add('assets/plugins/select2/css/select2.min.css');
				$this->template->stylesheet->add('assets/plugins/select2/css/select2-bootstrap.min.css');
				$this->template->javascript->add('assets/plugins/select2/js/select2.full.min.js');
				$this->template->javascript->add('assets/scripts/components-select2.js');


				$this->template->stylesheet->add('assets/css/components.css');
				$this->template->stylesheet->add('assets/css/plugins.min.css');


				$this->template->javascript->add('assets/plugins/jquery-validation/js/jquery.validate.min.js');
				$this->template->javascript->add('assets/plugins/jquery-validation/js/additional-methods.min.js');
				$this->template->javascript->add('assets/scripts/form-validation.js');

				$this->template->javascript->add('assets/scripts/map.js');

			// Dynamically add a css stylesheet
			//$this->template->stylesheet->add('https://openlayers.org/en/v4.0.1/css/ol.css');
			//$this->template->javascript->add('assets/scripts/map.js');

			$this->data['maps'] = $this->Home_model->GetMarker(false,true);
			$this->data['maps_cell'] = $this->Home_model->GetAll();




			//$this->data['title'] = $this->lang->line('create_user_heading');

			$tables = $this->config->item('tables','ion_auth');
			$identity_column = $this->config->item('identity','ion_auth');
			$this->data['identity_column'] = $identity_column;

			// validate form input
			$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
			$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
			if($identity_column!=='email')
			{
					$this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
					$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
			}
			else
			{
					$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
			}
			$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
			$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
			$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
			$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

			if ($this->form_validation->run() == true)
			{
					$email    = strtolower($this->input->post('email'));
					$identity = ($identity_column==='email') ? $email : $this->input->post('identity');
					$password = $this->input->post('password');

					$additional_data = array(
							'first_name' => $this->input->post('first_name'),
							'last_name'  => $this->input->post('last_name'),
							'company'    => $this->input->post('company'),
							'phone'      => $this->input->post('phone'),
					);
			}
			if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
			{
					// check to see if we are creating the user
					// redirect them back to the admin page
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("auth", 'refresh');
			}
			else
			{
					// display the create user form
					// set the flash data error message if there is one
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

					$this->data['first_name'] = array(
							'name'  => 'first_name',
							'id'    => 'first_name',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> 'true',
							'value' => $this->form_validation->set_value('first_name'),
					);
					$this->data['last_name'] = array(
							'name'  => 'last_name',
							'id'    => 'last_name',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> 'true',
							'value' => $this->form_validation->set_value('last_name'),
					);
					$this->data['identity'] = array(
							'name'  => 'identity',
							'id'    => 'identity',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('identity'),
					);
					$this->data['email'] = array(
							'name'  => 'email',
							'id'    => 'email',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('email'),
					);
					$this->data['company'] = array(
							'name'  => 'company',
							'id'    => 'company',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('company'),
					);
					$this->data['phone'] = array(
							'name'  => 'phone',
							'id'    => 'phone',
							'type'  => 'text',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('phone'),
					);
					$this->data['password'] = array(
							'name'  => 'password',
							'id'    => 'password',
							'type'  => 'password',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('password'),
					);
					$this->data['password_confirm'] = array(
							'name'  => 'password_confirm',
							'id'    => 'password_confirm',
							'type'  => 'password',
							'class' => "form-control input-sm",
							'required'=> '',
							'value' => $this->form_validation->set_value('password_confirm'),
					);

					//$this->_render_page('auth/create_user', $this->data);
			}



			//$data = array('titlemarwa' => 'Hello, world!'); // load from model (but using a dummy array here)
			$this->template->content->view('administration/index', $this->data);


			// Publish the template
			$this->template->publish();

	}


	// edit a user
	public function profile()
	{

				$this->template->javascript->add('assets/plugins/jquery-validation/js/jquery.validate.min.js');
				$this->template->javascript->add('assets/plugins/jquery-validation/js/additional-methods.min.js');
				$this->template->javascript->add('assets/scripts/form-validation.js');


		// Set the title
        $this->template->title = $this->lang->line('edit_user_heading');
        $this->template->pagename = '';


		$id = $this->ion_auth->user()->row()->id;

		if (!$this->ion_auth->logged_in())
		{
			redirect('administration/index', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('administration/profile', 'refresh');
					}
					else
					{
						redirect('administration/profile', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('administration/profile', 'refresh');
					}
					else
					{
						redirect('administration/profile', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'class' => "form-control input-sm",
			'required'=> 'true',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'class' => "form-control input-sm",
			'required'=> 'true',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'class' => "form-control input-sm",
			'required'=> 'true',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'class' => "form-control input-sm",
			'required'=> 'true',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'class' => "form-control input-sm",
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'class' => "form-control input-sm",
			'type' => 'password'
		);

		//$this->_render_page('auth/edit_user', $this->data);

		$this->template->content->view('administration/profile', $this->data);
		$this->template->publish();

	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	public	function ExcelDataAdd()	{  

		//phpinfo();
		//var_dump(extension_loaded ('zip'));
		//var_dump(get_loaded_extensions());

//Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)	 
         $configUpload['upload_path'] = FCPATH.'uploads/excel/';
         $configUpload['allowed_types'] = 'xls|xlsx|csv';
         $configUpload['max_size'] = '5000';
         $this->load->library('upload', $configUpload);
         $this->upload->do_upload('userfile');	
         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
         $file_name = $upload_data['file_name']; //uploded file name
		 $extension=$upload_data['file_ext'];    // uploded file extension
		
		//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
		 //$zipClass = PHPExcel_Settings::getZipClass();
		 //PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);


//$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
 $objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
          //Set to read only
          $objReader->setReadDataOnly(true); 		  
        //Load excel file
		 $objPHPExcel=$objReader->load(FCPATH.'uploads/excel/'.$file_name);		 
         $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
         $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          //loop from first data untill last data
          for($i=2;$i<=$totalrows;$i++)
          {
              $FirstName= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();			
              $LastName= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
			  $Email= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
			  $Mobile=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
			  $Address=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 4
			  $data_user=array('FirstName'=>$FirstName, 'LastName'=>$LastName ,'Email'=>$Email ,'Mobile'=>$Mobile , 'Address'=>$Address);
			  //$this->Home_model->Add_User($data_user);

			  var_dump( $data_user);
              
						  
          }
             unlink('././uploads/excel/'.$file_name); //File Deleted After uploading in database .			 
             redirect('administration/index', 'refresh');
	           
       
     }



     function importcsv() {

		// Set the title
        $this->template->title = 'Administartion';
        $this->template->pagename = 'admin';

             	
       // $data['addressbook'] = $this->csv_model->get_addressbook();
        $this->data['error'] = '';    //initialize image upload error array to empty
 
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
 
        $this->load->library('upload', $config);
 
 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();
 
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
 
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $row) {

                    $insert_data = array(
						'site_name'=>$row['﻿site_name'],
						'cell_phy'=>$row['cell_phy'],
						'Cell_code'=>$row['Cell_code'],
						'VENDOR'=>$row['VENDOR'],
						'PCI'=>$row['PCI'],
						'RSI'=>$row['RSI'],
						'eNodeB_ID'=>$row['eNodeB_ID'],
						'grp_AssigPU'=>$row['grp_AssigPUSCH'],
						'TAC'=>$row['TAC'],
						'BAND_4G'=>$row['BAND_4G'],
						'SITE_LOG'=>$row['SITE_LOG'],
						'X'=>str_replace(',', '.', $row['X']),
						'Y'=>str_replace(',', '.', $row['Y']),
						'AZ'=>$row['AZ'],
						'UARFCN'=>$row['UARFCN'],
						'Zone'=>$row['Zone'],
						'Statut'=>$row['Statut']
                    );

                    //var_dump($insert_data);
                    $this->Home_model->insert_csv($insert_data);
                }
                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect('administration/index', 'refresh');
                //echo "<pre>"; print_r($insert_data);
            } else 
                $this->data['error'] = "Error occured";
            }


            $this->template->content->view('administration/index', $this->data);

			// Publish the template
			$this->template->publish();
 
        } 

function export($table_name)  {

        $query = $this->Home_model->GetQuery($table_name);
        if(!$query)
            return false;
 

        // Starting the PHPExcel library
        //$this->load->library('PHPExcel');
        //$this->load->library('PHPExcel/IOFactory');
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);

         //Set Title
        $objPHPExcel->getActiveSheet()->setTitle('Data Absen');

        

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); //Excel5 - Excel2007
 
        //Header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        //header('Content-Type: application/vnd.ms-excel');

        //Nama File
        header('Content-Disposition: attachment;filename="'.$table_name.'_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');

 
        $objWriter->save('php://output');
    }

	
	public function get_cell_code(){
		$Cell_code = $this->input->post('Cell_code');

		$response = $this->Home_model->getCellCode($Cell_code);

		header("Content-type: text/json");
		header("Content-type: application/json");
		echo json_encode($response,true);

	}

	public function update_cell_code(){
		$update_cell  = array(
		  'Cell_code' 	=> $this->input->post('map_cell_code'),
		  'X' 	 		=> $this->input->post('X'),
		  'Y'	 		=> $this->input->post('Y'),
		  'AZ'		 	=> $this->input->post('AZ'),
		  'PCI'	 		=> $this->input->post('PCI'),
		  'RSI'	 		=> $this->input->post('RSI'),
		  'grp_AssigPU'	=> $this->input->post('grp_AssigPU')
		);

		echo $this->Home_model->UpdateCell($update_cell);
	}

	
}
