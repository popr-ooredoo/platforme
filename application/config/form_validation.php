<?php

    $config = array(
        'form_add_site' => array(
            array(
            'field' => 'site_name',
            'label' => 'Site name',
            'rules' => 'required',
            ),
            array(
            'field' => 'X',
            'label' => 'Lat',
            'rules' => 'required'
            ),
            array(
            'field' => 'Y',
            'label' => 'Long',
            'rules' => 'required'
            )
        ),
        'form_update_site' => array(
            array(
            'field' => 'pci_config_s1',
            'label' => 'pci_config_s1',
            'rules' => 'required',
            ),
            array(
            'field' => 'pci_config_s2',
            'label' => 'pci_config_s2',
            'rules' => 'required'
            ),
            array(
            'field' => 'rsi_config_s3',
            'label' => 'rsi_config_s3',
            'rules' => 'required'
            )
        ),
        'reviews_form' => array(
            array(
                'field' => 'review_rating',
                'label' => lang('review_rating'),
                'rules' => 'trim',
            ),
            array(
                'field' => 'review_meeting',
                'label' => lang('review_meeting'),
                'rules' => 'trim'
            )
        )

    );
