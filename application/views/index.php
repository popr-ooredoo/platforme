<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <style>
      #mapdiv {
        position: relative;
		height:100vh;
      }
    </style>


<div class="row">
<div class="col-md-8">
    <div id="mapdiv"></div>
</div>
<div class="col-md-4">

  <div class="form-group">
    <label for="single-prepend-text" class="control-label">Recherche</label>
    <div class="input-group select2-bootstrap-prepend">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
        <select id="single-prepend-text" class="form-control select2" onchange="gotomarker(this.value)">
          <option value="">Select one</option>
          <?php foreach ($maps as $key=>$map): ?>
                <option value="<?=$map->X ?>,<?=$map->Y ?>,<?=$key ?>"><?=$map->site_name ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>
<div id="status"></div>



  <div class="portlet box purple">
                                  <div class="portlet-title">
                                      <div class="caption">
                                          <i class="fa fa-gift"></i>Planning LTE </div>
                                      <div class="tools">
                                          <a href="javascript:;" class="collapse"> </a>
                                          <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                          <a href="javascript:;" class="reload"> </a>
                                          <a href="javascript:;" class="remove"> </a>
                                      </div>
                                  </div>
                                  <div class="portlet-body">
                                      <div class="panel-group accordion scrollable" id="accordion2">
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">  Layer </a>
                                                  </h4>
                                              </div>
                                              <div id="collapse_2_1" class="panel-collapse in">
                                                  <div class="panel-body">
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                          </p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2"> Collapsible Group Item #2 </a>
                                                  </h4>
                                              </div>
                                              <div id="collapse_2_2" class="panel-collapse collapse">
                                                  <div class="panel-body">
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                          </p>
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p>
                                                          <a class="btn blue" href="ui_tabs_accordions_navs.html#collapse_2_2" target="_blank"> Activate this section via URL </a>
                                                      </p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3"> Collapsible Group Item #3 </a>
                                                  </h4>
                                              </div>
                                              <div id="collapse_2_3" class="panel-collapse collapse">
                                                  <div class="panel-body">
                                                      <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                          Brunch 3 wolf moon tempor. </p>
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p>
                                                          <a class="btn green" href="ui_tabs_accordions_navs.html#collapse_2_3" target="_blank"> Activate this section via URL </a>
                                                      </p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="panel panel-default">
                                              <div class="panel-heading">
                                                  <h4 class="panel-title">
                                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_4"> Collapsible Group Item #4 </a>
                                                  </h4>
                                              </div>
                                              <div id="collapse_2_4" class="panel-collapse collapse">
                                                  <div class="panel-body">
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                          Brunch 3 wolf moon tempor. </p>
                                                      <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                      <p>
                                                          <a class="btn red" href="ui_tabs_accordions_navs.html#collapse_2_4" target="_blank"> Activate this section via URL </a>
                                                      </p>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
</div>
</div>






   <script src="/assets/scripts/OpenLayers.js"></script>
  <!-- <script src="http://www.openlayers.org/api/OpenLayers.js"></script> -->

  <script src="/assets/scripts/OpenLayers.js"></script>
  <script>
/*

  var layerListeners = {
    featureclick: function(e) {
        log(e.object.name + " says: " + e.feature.id + " clicked.");
        return false;
    },
    nofeatureclick: function(e) {
        log(e.object.name + " says: No feature clicked.");
    }
};


function log(msg) {
    if (!log.timer) {
        result.innerHTML = "";
        log.timer = window.setTimeout(function() {delete log.timer;}, 100);
    }
    result.innerHTML += msg + "<br>";
}
*/


map = new OpenLayers.Map({
    div: "mapdiv",
    projection: "EPSG:900913",
    controls: [],
    fractionalZoom: true
});


  //map = new OpenLayers.Map("mapdiv");
      map.addLayer(new OpenLayers.Layer.OSM());

      map.addControls([
              new OpenLayers.Control.Navigation(),
              new OpenLayers.Control.Attribution(),
              new OpenLayers.Control.PanZoomBar()
      ]);


      epsg4326 =  new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
      projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)

      var lonLat = new OpenLayers.LonLat( 10.2275, 36.7625 ).transform(epsg4326, projectTo);


      var zoom=8;
      map.setCenter (lonLat, zoom);

      var vectorLayer0 = new OpenLayers.Layer.Vector("Line");
      var vectorLayer1 = new OpenLayers.Layer.Vector("BAND 4G 1800");
      var vectorLayer2 = new OpenLayers.Layer.Vector("BAND 4G 800");
      var vectorLayer3 = new OpenLayers.Layer.Vector("Site", {
                      styleMap: new OpenLayers.StyleMap({
                          "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                            externalGraphic: 'assets/img/round.png',
                            graphicHeight: 16,
                            graphicWidth: 16,
                            fillOpacity: 1
                          }, OpenLayers.Feature.Vector.style["default"])),
                          "select": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                            externalGraphic: 'assets/img/round2.png',
                            graphicHeight: 16,
                            graphicWidth: 16,
                            fillOpacity: 1
                          }, OpenLayers.Feature.Vector.style["select"]))
                      })
                  });



      // Define markers as "features" of the vector layer:






      <?php
        foreach ($maps_1800 as $map):
       ?>
        var feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
                {site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
                {externalGraphic: 'assets/img/arrowx.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
            );
        vectorLayer1.addFeatures(feature);
        <?php endforeach ?>



        <?php
          foreach ($maps_800 as $map):
         ?>
          var feature = new OpenLayers.Feature.Vector(
                  new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
                  {site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
                  {externalGraphic: 'assets/img/arrow.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
              );
          vectorLayer2.addFeatures(feature);
          <?php endforeach ?>



          <?php foreach ($maps as $map): ?>
            var feature = new OpenLayers.Feature.Vector(
                    new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
                    {site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "}
                );
            vectorLayer3.addFeatures(feature);
            <?php endforeach ?>



            map.addLayers([vectorLayer1, vectorLayer2, vectorLayer3]);




            map.addControl(new OpenLayers.Control.LayerSwitcher());

                        selectControl = new OpenLayers.Control.SelectFeature(
                            [vectorLayer1, vectorLayer2, vectorLayer3],
                            {
                                clickout: true, toggle: false,
                                multiple: false, hover: false,
                                toggleKey: "ctrlKey", // ctrl key removes from selection
                                multipleKey: "shiftKey" // shift key adds to selection
                            }
                        );

                        map.addControl(selectControl);
                        selectControl.activate();
                        //map.setCenter(new OpenLayers.LonLat(0, 0), 3);
                        //vectors1.addFeatures(createFeatures());
                        //vectors2.addFeatures(createFeatures());

                        vectorLayer1.events.on({
                            "featureselected": function(e) {
                                createPopup(e.feature);
                            },
                            "featureunselected": function(e) {
                                destroyPopup(e.feature);
                            }
                        });

                        vectorLayer2.events.on({
                            "featureselected": function(e) {
                                createPopup(e.feature);
                            },
                            "featureunselected": function(e) {
                                destroyPopup(e.feature);
                            }
                        });

                        var p1 = null,
                        p2 = null,
                        len = null,
                        sitename = null;

                        vectorLayer3.events.on({
                            "featureselected": function(e) {
                                createPopup(e.feature);
                               var point = e.feature.geometry.getBounds().getCenterLonLat();

                              if(p1==null){
                                 p1 = new OpenLayers.Geometry.Point(point.lat,point.lon);
                                 sitename = e.feature.attributes.site_name;
                               } else {
                                 p2 = new OpenLayers.Geometry.Point(point.lat,point.lon);
                                 var distance = p1.distanceTo(p2)/1000;
                                  //len = Math.floor((p1.distanceTo(p2)/1000),2);
                                  len = distance.toFixed(3)
                                  showStatus("Distance entre <strong>"+sitename+"</strong> et <strong>"+e.feature.attributes.site_name+"</strong> = " + len + " Km");



                                  //make the line:
                                  var line=new OpenLayers.Geometry.LineString([p1, p2]);

                                  //style
                                  var style={strokeColor:"#0500bd", strokeWidth:3};
                                  //make vector
                                  var fea=new OpenLayers.Feature.Vector(line, {}, style);

                                  //add the feature
                                  vectorLayer0.addFeatures(fea);

                                  console.log(vectorLayer0);

                                  //add to map
                                  map.addLayer(vectorLayer0);



                                  p1=null;
                                  p2=null;
                                  len=null;
                                  sitename=null;
                              }

                            },
                            "featureunselected": function(e) {
                                destroyPopup(e.feature);
                            }
                        });




var lineLayer = new OpenLayers.Layer.Vector("Line Layer");
map.addLayer(lineLayer);
map.addControl(new OpenLayers.Control.DrawFeature(lineLayer, OpenLayers.Handler.Path));
var points = new Array(
   new OpenLayers.Geometry.Point(0, 10),
   new OpenLayers.Geometry.Point(30, 0)
);

var line = new OpenLayers.Geometry.LineString(points);

var style = {
  strokeColor: '#0000ff',
  strokeOpacity: 0.5,
  strokeWidth: 5
};

var lineFeature = new OpenLayers.Feature.Vector(line, null, style);
lineLayer.addFeatures([lineFeature]);

/*

                        measureControl = new OpenLayers.Control.Measure(
                            OpenLayers.Handler.Path,
                            {
                                immediate: true,
                                persist: true
                            }
                        );

                        measureControl.getCustomLength = function(evt, onlySum) {
                            var idx = evt.geometry.components.length;

                            if (idx < 2)
                                return '';

                            var geom = new OpenLayers.Geometry.LineString([
                                evt.geometry.components[idx - 2],
                                evt.geometry.components[idx - 1]
                            ]);

                            var lastLengthArr = this.getBestLength(geom);

                            var str = '';

                            if (idx > 2 || onlySum)
                                str += 'Total: ' + evt.measure.toFixed(3) + ' ' + evt.units + '&nbsp;&nbsp;&nbsp;';

                            if (!onlySum)
                                str += 'Last segment: ' +  lastLengthArr[0].toFixed(3)  + ' ' + lastLengthArr[1];

                                if(idx ==3)
                                measureControl.measureComplete();

                            return str;
                        }

                        map.addControl(measureControl);

                        // Update content in .foo div
                        measureControl.events.on({
                            'measure': function(evt) {
                                showStatus(measureControl.getCustomLength(evt, true));
                            },
                            'measurepartial': function(evt) {
                                showStatus(measureControl.getCustomLength(evt, false));
                            }
                        });

                        //measureControl.displaySystem = 'metric';

                        measureControl.activate();

*/
                    function showStatus(text) {
                        document.getElementById("status").innerHTML = text;
                    }





                          function createPopup(feature,close_last=false) {
                            //console.log(feature.attributes.site_name);
                            console.log(feature);
                            console.log(last_features);
                            if(last_features != null && last_features != feature && close_last==true){
                              destroyPopup(last_features);
                            }

                            feature.popup = new OpenLayers.Popup.FramedCloud("pop",
                                feature.geometry.getBounds().getCenterLonLat(),
                                null,
                                '<div class="markerContent">'+feature.attributes.description+'</div>',
                                null,
                                true,
                                function() {
                                  if(last_features != null && close_last==true){
                                    destroyPopup(last_features);
                                    last_features = null;
                                } else {
                                    selectControl.unselectAll();
                                }
                               }

                            );
                            //feature.popup.closeOnMove = true;
                            map.addPopup(feature.popup);
                            last_features = feature;

                          }

/*
                          function createPopup(feature,close_last=false) {
                            //console.log(feature.attributes.site_name);
                            feature.popup = new OpenLayers.Popup.FramedCloud("pop",
                                feature.geometry.getBounds().getCenterLonLat(),
                                null,
                                '<div class="markerContent">'+feature.attributes.description+'</div>',
                                null,
                                true,
                                function() {
                                    selectControl.unselectAll();
                               }
                            );
                            //feature.popup.closeOnMove = true;
                            map.addPopup(feature.popup);
                          }
                          */

                          function createPopup(feature,close_last=false) {
                            //console.log(feature.attributes.site_name);
                            if(last_features != null && last_features != false && last_features != feature ){
                              destroyPopup(last_features);
                              last_features = null;
                            }

                            feature.popup = new OpenLayers.Popup.FramedCloud("pop",
                                feature.geometry.getBounds().getCenterLonLat(),
                                null,
                                '<div class="markerContent">'+feature.attributes.description+'</div>',
                                null,
                                true,
                                function() {
                                  if(last_features != null && close_last==true){
                                    destroyPopup(last_features);
                                    last_features = null;
                                } else {
                                    selectControl.unselectAll();
                                }
                               }

                            );
                            //feature.popup.closeOnMove = true;
                            map.addPopup(feature.popup);
                            last_features = false;

                          }

                          function destroyPopup(feature) {
                            feature.popup.destroy();
                            feature.popup = null;
                          }

                            var last_features = null;

                          function gotomarker(value) {
                              var longlat = value.split(",");
                          //    map.panTo(lonLat);
                            map.setCenter (
                              new OpenLayers.LonLat( longlat[0] , longlat[1] )
                              .transform(epsg4326, projectTo),
                               15);

                               if(last_features == false) {
                                 selectControl.unselectAll();
                               }


                               var new_features = vectorLayer3.features[longlat[2]];
                               createPopup(new_features,true);
                               last_features = new_features;
                          }








/*
                          function CenterMap(longlat) {
                              map.getView().setCenter(ol.proj.transform([longlat], 'EPSG:4326', 'EPSG:3857'));
                              map.getView().setZoom(5);
                          }




      //Add a selector control to the vectorLayer with popup functions
      var controls = {
        selector: new OpenLayers.Control.SelectFeature(vectorLayer, { onSelect: createPopup, onUnselect: destroyPopup }),
        selector2: new OpenLayers.Control.SelectFeature(vectorLayer2, { onSelect: createPopup2, onUnselect: destroyPopup })
      };




      function createPopup2(feature) {
        feature.popup = new OpenLayers.Popup.FramedCloud("pop",
            feature.geometry.getBounds().getCenterLonLat(),
            null,
            '<div class="markerContent">hhhhhh</div>',
            null,
            true,
            function() { controls['selector2'].unselectAll(); }
        );
        //feature.popup.closeOnMove = true;
        map.addPopup(feature.popup);
      }


      map.addControl(controls['selector']);
      map.addControl(controls['selector2']);
      controls['selector'].activate();

*/





  </script>
