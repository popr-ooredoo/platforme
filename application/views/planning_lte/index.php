	<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	?>

	<style>
		#mapdiv {
			position: relative;
			height:100vh;
		}
	</style>

	<div class="row">
	<div class="col-md-12">
	<div class="portlet light portlet-fit bordered">

	<div class="portlet-title">
		<div class="caption">
			<i class=" icon-pointer font-green"></i>
			<span class="caption-subject font-green bold uppercase">Planning LTE</span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse"> </a>
			<a href="" class="fullscreen"> </a>
		</div>
	</div>

	<div class="portlet-body">
	<div class="row">

	<div class="col-md-8">
		<div id="mapdiv"></div>
		<div id="status"></div>
	</div>

	<div class="col-md-4">

		<div class="form-group">
			<div class="input-group select2-bootstrap-prepend">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</span>
				<select id="search_site" class="form-control select2 input-sm" onchange="gotomarker(this.value)">
					<option value="">Recherche Site</option>
					<?php foreach ($maps as $key=>$map): ?>
					<option value="<?=$map->X ?>,<?=$map->Y ?>,<?=$key ?>"><?=$map->site_name ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>

	<div class="panel-group accordion scrollable" id="accordion">

	<div class="panel panel-default">

	<div class="panel-heading">
		<h4 class="panel-title">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="<?=current_url(); ?>#add_site"> Créer Site </a>
		</h4>
	</div>

	<div id="add_site" class="panel-collapse <?php echo (!isset($pci) and !isset($pci_group) and !isset($pci_sector) and !isset($rsi) and !isset($u))?'in':'collapse'; ?>">

	<div class="panel-body">

	<form role="form" class="form-horizontal" name="form_add_site" id="form_add_site" method="post" action="">
		<div class="form-body">

		<input type="hidden" name="form_name" id="form_add_site" value="form_add_site">

			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below.
			</div>

			<div class="alert alert-success display-hide">
				<button class="close" data-close="alert"></button> Your form validation is successful! 
			</div>

			<?php if (isset($form_add_site_errors)) echo $form_add_site_errors; ?>


			<div class="form-group">
				<label class="col-md-3 control-label">Name <span class="required"> * </span></label>
				<div class="col-md-9">
					<input type="text" name="site_name" required="true" class="form-control input-sm" placeholder="Site name">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Lat <span class="required"> * </span></label>
				<div class="col-md-9">
					<input type="text" name="X" required="true" class="form-control input-sm" placeholder="X">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Long <span class="required"> * </span></label>
				<div class="col-md-9">
					<input type="text" name="Y" required="true" class="form-control input-sm" placeholder="Y">
				</div>
			</div>


			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-9">
					<div class="mt-checkbox-list">

						<label class="mt-checkbox mt-checkbox-outline"> Band 1800
							<input type="checkbox" name="BAND_4G[]" value="1800" />
							<span></span>
						</label>

						<label class="mt-checkbox mt-checkbox-outline"> Band 800
							<input type="checkbox" name="BAND_4G[]" value="800" />
							<span></span>
						</label>

					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Nb Sect</label>
				<div class="col-md-9">
					<select class="form-control input-sm" name="cell_phy" id="cell_phy">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
				</div>
			</div>


			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th> # </th>
						<th> AZ </th>
						<th> PCI </th>
						<th> RSI </th>
						<th> GrpA </th>
					</tr>
				</thead>

				<tbody>
					<tr id="L1">
						<td> S1 </td>
						<td> <input type="text" name="L1_AZ" class="form-control input-sm"> </td>
						<td> <input type="text" name="L1_PCI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L1_RSI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L1_GrpA" class="form-control input-sm"> </td>
					</tr>
					<tr id="L2">
						<td> S2 </td>
						<td> <input type="text" name="L2_AZ" class="form-control input-sm"> </td>
						<td> <input type="text" name="L2_PCI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L2_RSI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L2_GrpA" class="form-control input-sm"> </td>
					</tr>
					<tr id="L3">
						<td> S3 </td>
						<td> <input type="text" name="L3_AZ" class="form-control input-sm"> </td>
						<td> <input type="text" name="L3_PCI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L3_RSI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L3_GrpA" class="form-control input-sm"> </td>
					</tr>
					<tr id="L4">
						<td> S4 </td>
						<td> <input type="text" name="L4_AZ" class="form-control input-sm"> </td>
						<td> <input type="text" name="L4_PCI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L4_RSI" class="form-control input-sm"> </td>
						<td> <input type="text" name="L4_GrpA" class="form-control input-sm"> </td>
					</tr>
				</tbody>
			</table>

		</div>

		<div class="form-actions">
			<button type="submit" class="btn blue">OK</button>
		</div>

	</form>


	</div>
	</div>
	</div>
	<div class="panel panel-default">
	<div class="panel-heading">
	<h4 class="panel-title">
	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="<?=current_url(); ?>#plan_pci"> Plan PCI </a>
	</h4>
	</div>
	<div id="plan_pci" class="panel-collapse <?php echo (isset($pci) or isset($pci_group) or isset($pci_sector))?'in':'collapse'; ?>">
	<div class="panel-body">


	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-md-4 control-label">Plan</label>
			<div class="col-md-8">
				<select class="form-control input-sm" name="plan" id="plan">
					<option value="pci" <?php echo (isset($pci))?'selected':''; ?> >PCI</option>
					<option value="pci_group" <?php echo (isset($pci_group))?'selected':''; ?>>PCI Group</option>
					<option value="pci_sector" <?php echo (isset($pci_sector))?'selected':''; ?>>PCI Sector</option>
				</select>
			</div>
		</div>
	</div>

	<div id="div_pci">	
		<form role="form" class="form-horizontal" name="form_plan_pci" id="form_plan_pci" method="get" action="">

			<div class="form-group">
				<label class="col-md-4 control-label">PCI</label>
				<div class="col-md-8">
					<select class="form-control input-sm select2" name="pci" id="pci">
						<?php for($i=0;$i<504;$i++): ?>
						<option value="<?=$i ?>" <?php echo (isset($pci) and $i == $pci)?'selected':''; ?>><?=$i ?></option>
						<?php endfor ?>
					</select>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn green">OK</button>
			</div>

		</form>
	</div>


	<div id="div_pci_group">
		<form role="form" class="form-horizontal" name="form_plan_pci_group" id="form_plan_pci_group" method="get" action="">

			<div class="form-group">
				<label class="col-md-4 control-label">PCI Group</label>
				<div class="col-md-8">
					<select class="form-control input-sm select2" name="pci_group" id="pci_group">
						<?php for($i=0;$i<168;$i++): ?>
						<option value="<?=$i ?>" <?php echo (isset($pci_group) and $i == $pci_group)?'selected':''; ?>><?=$i ?></option>
						<?php endfor ?>
					</select>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn green">OK</button>
			</div>

		</form>
	</div>

	<div id="div_pci_sector">
		<form role="form" class="form-horizontal" name="form_plan_pci_sector" id="form_plan_pci_sector" method="get" action="">

			<div class="form-group">
				<label class="col-md-4 control-label">PCI Sector</label>
				<div class="col-md-8">
					<select class="form-control input-sm select2" name="pci_sector" id="pci_sector">
						<?php for($i=0;$i<3;$i++): ?>
						<option value="<?=$i ?>" <?php echo (isset($pci_sector) and $i == $pci_sector)?'selected':''; ?>><?=$i ?></option>
						<?php endfor ?>
					</select>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn green">OK</button>
			</div>

		</form>
	</div>


	</div>
	</div>
	</div>

	<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="<?=current_url(); ?>#plan_rsi"> Plan RSI </a>
			</h4>
		</div>

		<div id="plan_rsi" class="panel-collapse <?php echo (isset($rsi))?'in':'collapse'; ?>">
			<div class="panel-body">
				<form role="form" class="form-horizontal" name="form_plan_rsi" id="form_plan_rsi" method="get" action="">

					<div class="form-group">
						<label class="col-md-4 control-label">RSI</label>
						<div class="col-md-8">
							<select class="form-control input-sm select2" name="rsi" id="rsi">
								<?php for($i=0;$i<838;$i++): ?>
								<option value="<?=$i ?>" <?php echo (isset($rsi) and $i == $rsi)?'selected':''; ?>><?=$i ?></option>
								<?php endfor ?>
							</select>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn red">OK</button>
					</div>

				</form>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
	<div class="panel-heading">
	<h4 class="panel-title">
	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="<?=current_url(); ?>#plan_u"> Plan U </a>
	</h4>
	</div>
	<div id="plan_u" class="panel-collapse <?php echo (isset($u))?'in':'collapse'; ?>">
	<div class="panel-body">


	<form role="form" class="form-horizontal" name="form_plan_u" id="form_plan_u" method="get" action="">

	<div class="form-group">
	<label class="col-md-4 control-label">U</label>
	<div class="col-md-8">
	<select class="form-control input-sm select2" name="u" id="u">
	<?php for($i=0;$i<30;$i++): ?>
	<option value="<?=$i ?>" <?php echo (isset($u) and $i == $u)?'selected':''; ?>><?=$i ?></option>
	<?php endfor ?>
	</select>
	</div>
	</div>

	<div class="form-actions">
	<button type="submit" class="btn red">OK</button>
	</div>

	</form>


	</div>
	</div>
	</div>


	<div class="panel panel-default">
	<div class="panel-heading">
	<h4 class="panel-title">
	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_2_6"> Config New Site </a>
	</h4>
	</div>
	<div id="collapse_2_6" class="panel-collapse collapse">
	<div class="panel-body">


	<form role="form" class="form-horizontal" name="form_config_site" id="form_config_site" method="post" action="" onsubmit="return false;">
	<div class="form-body">


		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below.
		</div>

		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button> Your form validation is successful! 
		</div>

		<?php if (isset($form_update_site_errors)) echo $form_update_site_errors; ?>

		<div class="input-group select2-bootstrap-prepend">
			<span class="input-group-btn">
				<button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</span>
			<select id="sitename_config" name="sitename_config" class="form-control select2 input-sm" onchange="gotomarker2(this.value)">
				<option value="">Site Name</option>
				<?php foreach ($maps as $key=>$map): ?>
				<option value="<?=$map->X ?>,<?=$map->Y ?>,<?=$key ?>,<?=$map->site_name ?>"><?=$map->site_name ?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Distance</label>
			<div class="col-md-9">
				<select class="form-control input-sm" name="diastance_config" id="diastance_config" onchange="update_radius(this.value)">
					<option value="3000">3</option>
					<option value="5000">5</option>
					<option value="10000">10</option>
				</select>
			</div>
		</div>

<!--
		<div class="form-group">
		    <label class="col-md-3 control-label">Config</label>
		    <div class="col-md-9">
		        <div class="mt-radio-inline">
		            <label class="mt-radio">
		                <input type="radio" name="config" id="config_pci" value="pci" checked> PCI
		                <span></span>
		            </label>
		            <label class="mt-radio">
		                <input type="radio" name="config" id="config_rsi" value="rsi"> RSI
		                <span></span>
		            </label>
		            <label class="mt-radio">
		                <input type="radio" name="config" id="config_u" value="u"> U
		                <span></span>
		            </label>
		        </div>
		    </div>
		</div>

-->
		<div class="form-actions">
			<button type="submit" class="btn blue">Valider</button>
			<a class="btn red" id="delete_site"> Delete this Site </a>
		</div>

	</div>

	</form>

	<div id="block_suggestion" class="display-hide">

	<div class="text-center" style="margin: 15px 0;">------- Suggestion -------</div>

	<form role="form" class="form-horizontal" name="form_update_site" id="form_update_site" method="post" action="">
	<div class="form-body">

	<input type="hidden" name="form_name" id="form_name" value="form_update_site">
	<input type="hidden" name="site_name_config" id="site_name_config" value="">


 <div class="tabbable-line">
	<ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_1_1_1" data-toggle="tab"> PCI </a>
        </li>
        <li>
            <a href="#tab_1_1_2" data-toggle="tab"> RSI </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_1_1">

			<div class="form-group">
				<label class="col-md-4 control-label">Pci Group 1</label>
				<div class="col-md-8">
					<select class="form-control input-sm" name="pci_group_1" id="pci_group_1">
					</select>
				</div>			
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">Pci Group 2</label>
				<div class="col-md-8">
					<select class="form-control input-sm" name="pci_group_2" id="pci_group_2">
					</select>
				</div>
			</div>

        </div>
        <div class="tab-pane" id="tab_1_1_2">

			<div class="form-group">
				<label class="col-md-4 control-label">PrachCs</label>
				<div class="col-md-8">
					<select class="form-control input-sm" name="prachcs" id="prachcs">
						<option value="">Select PrachCs (12)</option>
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
					</select>
				</div>
			</div>

        </div>
    </div>
</div>




	<div id="block_config" class="display-hide">


		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th> # </th>
					<th> PCI </th>
					<th> RSI </th>
					<th> U </th>
				</tr>
			</thead>
			<tbody>
				<tr id="L1">
					<td> S1 </td>
					<td> <select class="form-control input-sm" name="pci_config_s1" id="pci_config_s1"></select> </td>
					<td> <select class="form-control input-sm" name="rsi_config_s1" id="rsi_config_s1"></select> </td>
					<td> <select class="form-control input-sm" name="u_config_s1" id="u_config_s1"></select> </td>
				</tr>
				<tr id="L2">
					<td> S2 </td>
					<td> <select class="form-control input-sm" name="pci_config_s2" id="pci_config_s2"></select> </td>
					<td> <select class="form-control input-sm" name="rsi_config_s2" id="rsi_config_s2"></select> </td>
					<td> <select class="form-control input-sm" name="u_config_s2" id="u_config_s2"></select> </td>
				</tr>
				<tr id="L3">
					<td> S3 </td>
					<td> <select class="form-control input-sm" name="pci_config_s3" id="pci_config_s3"></select> </td>
					<td> <select class="form-control input-sm" name="rsi_config_s3" id="rsi_config_s3"></select> </td>
					<td> <select class="form-control input-sm" name="u_config_s3" id="u_config_s3"></select> </td>
				</tr>
				<tr id="L4">
					<td> S4 </td>
					<td> <select class="form-control input-sm" name="pci_config_s4" id="pci_config_s4"></select> </td>
					<td> <select class="form-control input-sm" name="rsi_config_s4" id="rsi_config_s4"></select> </td>
					<td> <select class="form-control input-sm" name="u_config_s4" id="u_config_s4"></select> </td>
				</tr>
			</tbody>
		</table>

		<div class="form-actions">
			<button type="submit" class="btn green">Update Site</button>
		</div>

	</div>

	</div>
	</form>


	</div>








	</div>
	</div>
	</div>



	<div class="panel panel-default">
	<div class="panel-heading">
	<h4 class="panel-title">
	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_2_8"> Export Database </a>
	</h4>
	</div>

		<div id="collapse_2_8" class="panel-collapse collapse">
			<div class="panel-body">
				<a class="btn red" href="administration/export/map" title="Export data"> Exporter </a>
			</div>
		</div>

	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<script src="/assets/scripts/OpenLayers.js"></script>
	<!-- <script src="http://www.openlayers.org/api/OpenLayers.js"></script> -->
	<script>
	/*

	var layerListeners = {
	featureclick: function(e) {
	log(e.object.name + " says: " + e.feature.id + " clicked.");
	return false;
	},
	nofeatureclick: function(e) {
	log(e.object.name + " says: No feature clicked.");
	}
	};


	function log(msg) {
	if (!log.timer) {
	result.innerHTML = "";
	log.timer = window.setTimeout(function() {delete log.timer;}, 100);
	}
	result.innerHTML += msg + "<br>";
	}
	*/


	map = new OpenLayers.Map({
		div: "mapdiv",
		projection: "EPSG:900913",
		controls: [],
		fractionalZoom: true
	});


	//map = new OpenLayers.Map("mapdiv");
	map.addLayer(new OpenLayers.Layer.OSM());

	map.addControls([
		new OpenLayers.Control.Navigation(),
		new OpenLayers.Control.Attribution(),
		new OpenLayers.Control.PanZoomBar()
	]);


	epsg4326 =  new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
	projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)

	var lonLat = new OpenLayers.LonLat( 10.2275, 36.7625 ).transform(epsg4326, projectTo);


	var zoom=8;
	map.setCenter (lonLat, zoom);

	var vectorLayer0 = new OpenLayers.Layer.Vector("Line");
	var vectorLayer1 = new OpenLayers.Layer.Vector("BAND 4G 1800");
	var vectorLayer2 = new OpenLayers.Layer.Vector("BAND 4G 800");
	var vectorLayer3 = new OpenLayers.Layer.Vector("Site", {
		styleMap: new OpenLayers.StyleMap({
			"default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
				externalGraphic: 'assets/img/round.png',
				graphicHeight: 16,
				graphicWidth: 16,
				fillOpacity: 1
				}, OpenLayers.Feature.Vector.style["default"])),
			"select": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
				externalGraphic: 'assets/img/round2.png',
				graphicHeight: 16,
				graphicWidth: 16,
				fillOpacity: 1
			}, OpenLayers.Feature.Vector.style["select"]))
		})
	});



	// Define markers as "features" of the vector layer:
	<?php foreach ($maps_1800 as $map): ?>

	<?php if( isset($pci) and $map->PCI == $pci) { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx1.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($pci_group) and intdiv($map->PCI, 3) == $pci_group) {	?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx2.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($pci_sector) and ($map->PCI % 3) == $pci_sector) { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx3.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($rsi) and $map->RSI == $rsi) { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx4.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($u) and $map->grp_AssigPU == $u){ ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx5.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } else { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?> <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?>  <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrowx.png', graphicHeight: 42, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-42, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } ?>

	vectorLayer1.addFeatures(feature);
	<?php endforeach ?>



	<?php foreach ($maps_800 as $map): ?>

	<?php if( isset($pci) and $map->PCI == $pci){	?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow1.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($pci_group) and intdiv($map->PCI, 3) == $pci_group){	 ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow2.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($pci_sector) and ($map->PCI % 3) == $pci_sector){  ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow3.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } elseif( isset($rsi) and $map->RSI == $rsi) { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow4.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php	} elseif( isset($u) and $map->grp_AssigPU == $u) {	 ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow5.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } else { ?>

		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>cell_phy :</strong> <?=$map->cell_phy ?> <br><strong>Cell_code :</strong> <?=$map->Cell_code ?>  <br><strong>BAND_4G :</strong> <?=$map->BAND_4G ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>PCI :</strong> <?=$map->PCI ?> <br><strong>RSI :</strong> <?=$map->RSI ?> <br><strong>eNodeB_ID :</strong> <?=$map->eNodeB_ID ?> <br><strong>grp_AssigPU :</strong> <?=$map->grp_AssigPU ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>SITE_LOG :</strong> <?=$map->SITE_LOG ?> <br><strong>UARFCN :</strong> <?=$map->UARFCN ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "},
			{externalGraphic: 'assets/img/arrow.png', graphicHeight: 25, graphicWidth: 25, graphicXOffset:-12, graphicYOffset:-25, fillOpacity: 1, rotation:<?=$map->AZ ?> }
		);

	<?php } ?>

	vectorLayer2.addFeatures(feature);
	<?php endforeach ?>



	<?php foreach ($maps as $map): ?>
		var feature = new OpenLayers.Feature.Vector(
		new OpenLayers.Geometry.Point( <?=$map->X ?>, <?=$map->Y ?> ).transform(epsg4326, projectTo),
			{site_name:"<?=$map->site_name ?>",description:"<strong>site_name :</strong> <?=$map->site_name ?> <br><strong>VENDOR :</strong> <?=$map->VENDOR ?> <br><strong>TAC :</strong> <?=$map->TAC ?> <br><strong>Zone :</strong> <?=$map->Zone ?> <br><strong>Statut :</strong> <?=$map->Statut ?> "}
		);
		vectorLayer3.addFeatures(feature);
	<?php endforeach ?>



	map.addLayers([vectorLayer1, vectorLayer2, vectorLayer3]);


	map.addControl(new OpenLayers.Control.LayerSwitcher());

	selectControl = new OpenLayers.Control.SelectFeature(
		[vectorLayer1, vectorLayer2, vectorLayer3],
		{
			clickout: true, toggle: false,
			multiple: false, hover: false,
			toggleKey: "ctrlKey", // ctrl key removes from selection
			multipleKey: "shiftKey" // shift key adds to selection
		}
	);

	map.addControl(selectControl);
	selectControl.activate();
	//map.setCenter(new OpenLayers.LonLat(0, 0), 3);
	//vectors1.addFeatures(createFeatures());
	//vectors2.addFeatures(createFeatures());

	vectorLayer1.events.on({
		"featureselected": function(e) {
			createPopup(e.feature);
		},
		"featureunselected": function(e) {
			destroyPopup(e.feature);
		}
	});

	vectorLayer2.events.on({
		"featureselected": function(e) {
			createPopup(e.feature);
		},
		"featureunselected": function(e) {
			destroyPopup(e.feature);
		}
	});

	var p1 = null,
	p2 = null,
	len = null,
	sitename = null;

	vectorLayer3.events.on({
		"featureselected": function(e) {
		createPopup(e.feature);
			var point = e.feature.geometry.getBounds().getCenterLonLat();

			if(p1==null){
				p1 = new OpenLayers.Geometry.Point(point.lat,point.lon);
				sitename = e.feature.attributes.site_name;
			} else {
				p2 = new OpenLayers.Geometry.Point(point.lat,point.lon);
				var distance = p1.distanceTo(p2)/1000;
				//len = Math.floor((p1.distanceTo(p2)/1000),2);
				len = distance.toFixed(3)
				showStatus("Distance entre <strong>"+sitename+"</strong> et <strong>"+e.feature.attributes.site_name+"</strong> = " + len + " Km");

				//make the line:
				var line=new OpenLayers.Geometry.LineString([p1, p2]);

				//style
				var style={strokeColor:"#0500bd", strokeWidth:3};
				//make vector
				var fea=new OpenLayers.Feature.Vector(line, {}, style);

				//add the feature
				vectorLayer0.addFeatures(fea);

				console.log(vectorLayer0);

				//add to map
				map.addLayer(vectorLayer0);

				p1=null;
				p2=null;
				len=null;
				sitename=null;
			}
		},
		"featureunselected": function(e) {
			destroyPopup(e.feature);
		}
	});


/*

	var lineLayer = new OpenLayers.Layer.Vector("Line Layer");
	map.addLayer(lineLayer);
	map.addControl(new OpenLayers.Control.DrawFeature(lineLayer, OpenLayers.Handler.Path));
	var points = new Array(
		new OpenLayers.Geometry.Point(0, 10),
		new OpenLayers.Geometry.Point(30, 0)
	);

	var line = new OpenLayers.Geometry.LineString(points);

	var style = {
	strokeColor: '#0000ff',
	strokeOpacity: 0.5,
	strokeWidth: 5
	};

	var lineFeature = new OpenLayers.Feature.Vector(line, null, style);
	lineLayer.addFeatures([lineFeature]);
*/



	/*

	measureControl = new OpenLayers.Control.Measure(
	OpenLayers.Handler.Path,
	{
	immediate: true,
	persist: true
	}
	);

	measureControl.getCustomLength = function(evt, onlySum) {
	var idx = evt.geometry.components.length;

	if (idx < 2)
	return '';

	var geom = new OpenLayers.Geometry.LineString([
	evt.geometry.components[idx - 2],
	evt.geometry.components[idx - 1]
	]);

	var lastLengthArr = this.getBestLength(geom);

	var str = '';

	if (idx > 2 || onlySum)
	str += 'Total: ' + evt.measure.toFixed(3) + ' ' + evt.units + '&nbsp;&nbsp;&nbsp;';

	if (!onlySum)
	str += 'Last segment: ' +  lastLengthArr[0].toFixed(3)  + ' ' + lastLengthArr[1];

	if(idx ==3)
	measureControl.measureComplete();

	return str;
	}

	map.addControl(measureControl);

	// Update content in .foo div
	measureControl.events.on({
	'measure': function(evt) {
	showStatus(measureControl.getCustomLength(evt, true));
	},
	'measurepartial': function(evt) {
	showStatus(measureControl.getCustomLength(evt, false));
	}
	});

	//measureControl.displaySystem = 'metric';

	measureControl.activate();

	*/
	function showStatus(text) {
		//document.getElementById("status").innerHTML = text;
		bootbox.alert(text);
	}

	function createPopup(feature,close_last=false) {
		//console.log(feature.attributes.site_name);
		console.log(feature);
		console.log(last_features);
		if(last_features != null && last_features != feature && close_last==true){
			destroyPopup(last_features);
		}

		feature.popup = new OpenLayers.Popup.FramedCloud("pop",
			feature.geometry.getBounds().getCenterLonLat(),
			null,
			'<div class="markerContent">'+feature.attributes.description+'</div>',
			null,
			true,
			function() {
				if(last_features != null && close_last==true){
					destroyPopup(last_features);
					last_features = null;
				} else {
					selectControl.unselectAll();
				}
			}
		);
		//feature.popup.closeOnMove = true;
		map.addPopup(feature.popup);
		last_features = feature;

	}

	/*
	function createPopup(feature,close_last=false) {
	//console.log(feature.attributes.site_name);
	feature.popup = new OpenLayers.Popup.FramedCloud("pop",
	feature.geometry.getBounds().getCenterLonLat(),
	null,
	'<div class="markerContent">'+feature.attributes.description+'</div>',
	null,
	true,
	function() {
	selectControl.unselectAll();
	}
	);
	//feature.popup.closeOnMove = true;
	map.addPopup(feature.popup);
	}
	*/

	function createPopup(feature,close_last=false) {
		//console.log(feature.attributes.site_name);
		if(last_features != null && last_features != false && last_features != feature ){
			destroyPopup(last_features);
			last_features = null;
		}

		feature.popup = new OpenLayers.Popup.FramedCloud("pop",
			feature.geometry.getBounds().getCenterLonLat(),
			null,
			'<div class="markerContent">'+feature.attributes.description+'</div>',
			null,
			true,
			function() {
				if(last_features != null && close_last==true){
					destroyPopup(last_features);
					last_features = null;
				} else {
					selectControl.unselectAll();
				}
			}
		);

		//feature.popup.closeOnMove = true;
		map.addPopup(feature.popup);
		last_features = false;

	}

	function destroyPopup(feature) {
		feature.popup.destroy();
		feature.popup = null;
	}

	var last_features = null;

	function gotomarker(value) {
		var longlat = value.split(",");
		//    map.panTo(lonLat);
		map.setCenter (
			new OpenLayers.LonLat( longlat[0] , longlat[1] )
			.transform(epsg4326, projectTo),
			15
		);

		if(last_features == false) {
			selectControl.unselectAll();
		}


		var new_features = vectorLayer3.features[longlat[2]];
		createPopup(new_features,true);
		last_features = new_features;
	}


	function gotomarker2(value) {

		console.log(value);

		
		var zoom_carte = 14;
		switch ($("select#diastance_config").val()) {
		    case '3000':
		        zoom_carte = 14;
		        break; 
		    case '5000':
		        zoom_carte = 13;
		        break; 
		    default: 
		        zoom_carte = 12;
		}
		console.log(zoom_carte);


		var longlat = value.split(",");
		//    map.panTo(lonLat);
		map.setCenter (
			new OpenLayers.LonLat( longlat[0] , longlat[1] )
			.transform(epsg4326, projectTo),
			zoom_carte
		);

		//console.log(last_features);

		if(last_features == false) {
			selectControl.unselectAll();
		} 

		if(last_features != null) {
			vector.destroyFeatures();
		}


		//polygonControl.activate();

		var new_features = vectorLayer3.features[longlat[2]];
		createPopup(new_features,true);
		last_features = new_features;
    
    		//console.log(last_features);


	    var point_x = last_features.geometry.x;
        var point_y = last_features.geometry.y;
        var point_max = point_x + $("select#diastance_config").val();

		var mycircle = OpenLayers.Geometry.Polygon.createRegularPolygon(
			new OpenLayers.Geometry.Point(point_x , point_y),
			$("select#diastance_config").val(),
			40,
			0
		);

		//document.getElementById("radius").innerHTML
      
        //make two points at center and at the edge
        var startPoint = new OpenLayers.Geometry.Point(point_x, point_y);
        var endPoint = new OpenLayers.Geometry.Point(point_max, point_y);
        var radius= new OpenLayers.Geometry.LineString([startPoint, endPoint]);
        //calculate length. WARNING! The EPSG:900913 lengths are meaningless except around the equator. Either use a local coordinate system like UTM, or geodesic calculations.
        var len = Math.round(radius.getLength()).toString();

        console.log(len);

        var laenge;
	    if (len > 1000) {
	        laenge = len / 1000;
	        einheit = " km";
	    } else {
	        laenge = len;
	        einheit = " m";
	    }


        //style the radius
        var punktstyle = {
            strokeColor: "red",
            strokeWidth: 2,
            pointRadius: 5,
            fillOpacity: 0.2
        };
        var style = {
            strokeColor: "#0500bd",
            strokeWidth: 3,
            label: laenge + einheit,
            labelAlign: "left",
            labelXOffset: "20",
            labelYOffset: "10",
            labelOutlineColor: "white",
            labelOutlineWidth: 3
        };




        //add radius feature to radii layer
        var punkt1 = new OpenLayers.Feature.Vector(startPoint, {}, punktstyle);
        var fea = new OpenLayers.Feature.Vector(radius, { 'length': len }, style);

		var featurecircle = new OpenLayers.Feature.Vector(mycircle);
		vector.addFeatures([featurecircle,punkt1, fea]);     
		//vector.addFeatures([featurecircle]);     

	}



	function update_radius(value) {
		

		if(last_features != null) {

			console.log(last_features);
			console.log(value);


			var zoom_carte = 14;
			switch (value) {
			    case '3000':
			        zoom_carte = 14;
			        break; 
			    case '5000':
			        zoom_carte = 13;
			        break; 
			    default: 
			        zoom_carte = 12;
			}

			console.log(zoom_carte);


		    var point_x = last_features.geometry.x;
	        var point_y = last_features.geometry.y;
	        var point_max = point_x + value;

			map.setCenter(new OpenLayers.LonLat(point_x, point_y), zoom_carte);


			//vector.removeFeatures([featurecircle]);
			vector.destroyFeatures();

			var mycircle = OpenLayers.Geometry.Polygon.createRegularPolygon(
				new OpenLayers.Geometry.Point(point_x , point_y),
				value,
				40,
				0
			);



        //make two points at center and at the edge
        var startPoint = new OpenLayers.Geometry.Point(point_x, point_y);
        var endPoint = new OpenLayers.Geometry.Point(point_max, point_y);
        var radius= new OpenLayers.Geometry.LineString([startPoint, endPoint]);
        //calculate length. WARNING! The EPSG:900913 lengths are meaningless except around the equator. Either use a local coordinate system like UTM, or geodesic calculations.
        var len = Math.round(radius.getLength()).toString();

        console.log(len);

        var laenge;
	    if (len > 1000) {
	        laenge = len / 1000;
	        einheit = " km";
	    } else {
	        laenge = len;
	        einheit = " m";
	    }


        //style the radius
        var punktstyle = {
            strokeColor: "red",
            strokeWidth: 2,
            pointRadius: 5,
            fillOpacity: 0.2
        };
        var style = {
            strokeColor: "#0500bd",
            strokeWidth: 3,
            label: laenge + einheit,
            labelAlign: "left",
            labelXOffset: "20",
            labelYOffset: "10",
            labelOutlineColor: "white",
            labelOutlineWidth: 3
        };



        //add radius feature to radii layer
        var punkt1 = new OpenLayers.Feature.Vector(startPoint, {}, punktstyle);
        var fea = new OpenLayers.Feature.Vector(radius, { 'length': len }, style);


			featurecircle = new OpenLayers.Feature.Vector(mycircle);
			vector.addFeatures([featurecircle]);

		}
		

	}

/********************************************************************/


var vector = new OpenLayers.Layer.Vector("Radius");
map.addLayer(vector);


// var selected_polygon_style = {
//     strokeWidth: 5,
//     strokeColor: '#ff0000'
//     // add more styling key/value pairs as your need
// };

// featurecircle.style = selected_polygon_style;








/*



var circles = new OpenLayers.Layer.Vector("Kreise");
var radiuslayer = new OpenLayers.Layer.Vector("Radius");


var polygonControl = new OpenLayers.Control.DrawFeature(circles,
					OpenLayers.Handler.RegularPolygon,
                    {handlerOptions: {sides: 40, radius: 3000, angle: 0}});



//initialize a map

map.addLayers([circles, radiuslayer]);
map.addControl(polygonControl);



circles.events.on({
    'featureadded': function (e) {

        // DRY-principle not applied

        var f = e.feature;
        //calculate the min/max coordinates of a circle
        var minX = f.geometry.bounds.left;
        var minY = f.geometry.bounds.bottom;
        var maxX = f.geometry.bounds.right;
        var maxY = f.geometry.bounds.top;
        //calculate the center coordinates
        var startX = (minX + maxX) / 2;
        var startY = (minY + maxY) / 2;

        //make two points at center and at the edge
        var startPoint = new OpenLayers.Geometry.Point(startX, startY);
        var endPoint = new OpenLayers.Geometry.Point(maxX, startY);
        var radius = new OpenLayers.Geometry.LineString([startPoint, endPoint]);
        //calculate length. WARNING! The EPSG:900913 lengths are meaningless except around the equator. Either use a local coordinate system like UTM, or geodesic calculations.
        var len = Math.round(radius.getLength()).toString();
        //style the radius
        var punktstyle = {
            strokeColor: "red",
            strokeWidth: 2,
            pointRadius: 5,
            fillOpacity: 0.2
        };
        var style = {
            strokeColor: "#0500bd",
            strokeWidth: 3,
            label: len + " m",
            labelAlign: "left",
            labelXOffset: "20",
            labelYOffset: "10",
            labelOutlineColor: "white",
            labelOutlineWidth: 3
        };
        //add radius feature to radii layer
        var punkt1 = new OpenLayers.Feature.Vector(startPoint, {

        }, punktstyle);
        var fea = new OpenLayers.Feature.Vector(radius, {
            'length': len
        }, style);


        radiuslayer.addFeatures([punkt1, fea]);
    }
});


*/





	/*
	function CenterMap(longlat) {
	map.getView().setCenter(ol.proj.transform([longlat], 'EPSG:4326', 'EPSG:3857'));
	map.getView().setZoom(5);
	}




	//Add a selector control to the vectorLayer with popup functions
	var controls = {
	selector: new OpenLayers.Control.SelectFeature(vectorLayer, { onSelect: createPopup, onUnselect: destroyPopup }),
	selector2: new OpenLayers.Control.SelectFeature(vectorLayer2, { onSelect: createPopup2, onUnselect: destroyPopup })
	};




	function createPopup2(feature) {
	feature.popup = new OpenLayers.Popup.FramedCloud("pop",
	feature.geometry.getBounds().getCenterLonLat(),
	null,
	'<div class="markerContent">hhhhhh</div>',
	null,
	true,
	function() { controls['selector2'].unselectAll(); }
	);
	//feature.popup.closeOnMove = true;
	map.addPopup(feature.popup);
	}


	map.addControl(controls['selector']);
	map.addControl(controls['selector2']);
	controls['selector'].activate();

	*/



	</script>


    <style type="text/css">
    /*
        p {
            width: 512px;
        }
        #config {
            margin-top: 1em;
            width: 512px;
            position: relative;
            height: 8em;
        }
        #controls {
            padding-left: 2em;
            margin-left: 0;
            width: 12em;
        }
        #controls li {
            padding-top: 0.5em;
            list-style: none;
        }
        #options {
            font-size: 1em;
            top: 0;
            margin-left: 15em;
            position: absolute;
        }

        .olImageLoadError {
            background-color: transparent !important;
        }
        */
    </style>