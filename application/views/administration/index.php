<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="row">
<div class="col-md-12">
<div class="portlet light portlet-fit bordered">
<div class="portlet-title">
<div class="caption">
<i class=" icon-settings font-green"></i>
<span class="caption-subject font-green bold uppercase">Administration</span>
</div>
</div>
<div class="portlet-body">

<div class="tabbable-custom nav-justified">

  <ul class="nav nav-tabs nav-justified">
    <li class="active">
      <a href="#new_ws" data-toggle="tab"> Nouveau WS </a>
    </li>
    <li>
      <a href="#update_cellule" data-toggle="tab"> Mettre à jour une cellule </a>
    </li>
    <li>
      <a href="#create_user" data-toggle="tab"> Création compte </a>
    </li>
  </ul>

<div class="tab-content">

  <div class="tab-pane active" id="new_ws">

    <div class="panel-body">



      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">



        <?php echo form_open_multipart("administration/importcsv", array('class' => 'form-horizontal', 'id' => 'form_importcsv', 'name'=>"form_importcsv", 'method'=>"post"));?>


            <div class="form-body">                



      <?php if (isset($error)): ?>
        <div class="alert alert-danger">
          <button class="close" data-close="alert"></button>
          <?php echo $error; ?>
        </div>
      <?php endif; ?>

      <?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success">
          <button class="close" data-close="alert"></button>
          <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif; ?>




              <div class="form-group">
                <label for="exampleInputFile" class="col-md-4 control-label">File input CSV</label>
                <div class="col-md-8">
                  <input type="file" id="userfile" name="userfile">
                </div>
              </div>

          </div>

            <div class="form-actions">
              <input type="submit" class="btn blue" value="upload" name="Charger" />
            </div>

          <?php echo form_close(); ?>

        </div>
      </div>

    </div>

  </div>




<div class="tab-pane" id="update_cellule">


<div class="row">
<div class="col-lg-8 col-lg-offset-2">

<div class="panel-body">

<form role="form" class="form-horizontal" name="form_update_cell" id="form_update_cell" method="post" action="">
<div class="form-body">

      <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some form errors. Please check below.
      </div>

      <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! 
      </div>

      <?php echo validation_errors(); ?>

    <div class="form-group">
      <div class="input-group select2-bootstrap-prepend">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
        <select id="map_cell_code" name="map_cell_code" class="form-control select2-cell input-sm">
          <option value="">Recherche cell code</option>
          <?php foreach ($maps_cell as $key=>$map): ?>
          <option value="<?=$map->Cell_code ?>"><?=$map->Cell_code ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>


<div class="form-group">
  <label class="col-md-4 control-label">Lat <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="X" id="X" required="true" class="form-control input-sm" placeholder="X">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Long <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="Y" id="Y" required="true" class="form-control input-sm" placeholder="Y">
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label">AZ <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="AZ" id="AZ" required="true" class="form-control input-sm" placeholder="AZ">
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label">PCI <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="PCI" id="PCI" required="true" class="form-control input-sm" placeholder="PCI">
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label">RSI <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="RSI" id="RSI" required="true" class="form-control input-sm" placeholder="RSI">
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label">grp_AssigPU <span class="required"> * </span></label>
  <div class="col-md-8">
    <input type="text" name="grp_AssigPU" id="grp_AssigPU" required="true" class="form-control input-sm" placeholder="grp_AssigPU">
  </div>
</div>




</div>

  <div class="form-actions">
    <button type="submit" class="btn blue">Apply</button>
  </div>

</form>
</div>
</div>
</div>
</div>

<div class="tab-pane" id="create_user">

<div class="row">
<div class="col-lg-8 col-lg-offset-2">

<div class="panel-body">

<?php echo form_open("auth/create_user", array('class' => 'form-horizontal', 'id' => 'form_add_compte', 'name'=>"form_add_site", 'method'=>"post", 'role'=>"form"));?>

<div class="form-body">


      <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some form errors. Please check below.
      </div>

      <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! 
      </div>


  <?php if (isset($message)): ?>
    <div class="alert alert-info">
      <button class="close" data-close="alert"></button>
      <?php echo $message; ?>
    </div>
  <?php endif; ?>




<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_fname_label');?> <span class="required"> * </span></label>
<div class="col-md-8">
<?php echo form_input($first_name);?>
</div>
</div>


<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_lname_label');?> <span class="required"> * </span></label>
<div class="col-md-8">
<?php echo form_input($last_name);?>
</div>
</div>


<?php
if($identity_column!=='email') { ?>

<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_identity_label');?> </label>
<div class="col-md-8">
<?php echo form_input($identity);?>
</div>
</div>

<?php }
?>

<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_company_label');?> <span class="required"> * </span></label>
<div class="col-md-8">
<?php echo form_input($company);?>
</div>
</div>


<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_email_label');?> </label>
<div class="col-md-8">
<?php echo form_input($email);?>
</div>
</div>




<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_phone_label');?> </label>
<div class="col-md-8">
<?php echo form_input($phone);?>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_password_label');?> </label>
<div class="col-md-8">
<?php echo form_input($password);?>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label"><?php echo lang('create_user_password_confirm_label');?> </label>
<div class="col-md-8">
<?php echo form_input($password_confirm);?>
</div>
</div>


  <div class="form-actions">
    <?php echo form_submit('submit', lang('create_user_submit_btn'), array('class'=>"btn blue"));?>
  </div>


</div>
<?php echo form_close();?>

</div>
</div>
</div>

</div>
</div>
</div>





</div>
</div>
</div>
</div>
