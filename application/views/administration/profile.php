<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-bulb font-green"></i>
                    <span class="caption-subject font-green bold uppercase"><?php echo lang('edit_user_heading');?></span>
                </div>
            </div>
            <div class="portlet-body">
            <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

<?php echo form_open("auth/edit_user/".$this->ion_auth->user()->row()->id, array('class' => 'form-horizontal', 'id' => 'form_edit_compte', 'name'=>"form_edit_compte", 'method'=>"post", 'role'=>"form"));?>


<div class="form-body">


      <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some form errors. Please check below.
      </div>

      <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! 
      </div>


  <?php if (isset($message)): ?>
    <div class="alert alert-info">
      <button class="close" data-close="alert"></button>
      <?php echo $message; ?>
    </div>
  <?php endif; ?>



  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_fname_label');?> <span class="required"> * </span></label>
    <div class="col-md-7">
      <?php echo form_input($first_name);?>
    </div>
  </div>


  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_lname_label');?> <span class="required"> * </span></label>
    <div class="col-md-7">
      <?php echo form_input($last_name);?>
    </div>
  </div>


  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_company_label');?> <span class="required"> * </span></label>
    <div class="col-md-7">
      <?php echo form_input($company);?>
    </div>
  </div>


  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_phone_label');?> <span class="required"> * </span></label>
    <div class="col-md-7">
      <?php echo form_input($phone);?>
    </div>
  </div>


  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_password_label');?> </label>
    <div class="col-md-7">
      <?php echo form_input($password);?>
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-5 control-label"><?php echo lang('edit_user_password_confirm_label');?> </label>
    <div class="col-md-7">
      <?php echo form_input($password_confirm);?>
    </div>
  </div>


      <?php /* if ($this->ion_auth->is_admin()): ?>

  <div class="form-group">
<div class="col-md-12">
<fieldset>
    <legend><?php echo lang('edit_user_groups_heading');?></legend>

          <?php foreach ($groups as $group):?>

<div class="form-check">
    <label class="form-check-label">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
    </label>
  </div>

          <?php endforeach?>


  </fieldset>

  </div>
  </div>




      <?php endif */?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <div class="form-actions">
    <?php echo form_submit('submit', lang('edit_user_submit_btn'), array('class'=>"btn blue"));?>
  </div>


</div>

<?php echo form_close();?>




            </div>
            </div>

            </div>
        </div>
    </div>
</div>
