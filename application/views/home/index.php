<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!-- BEGIN : MORRIS CHARTS -->
<div class="row">


    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-home font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Distribution des sites (<?=$distribution['BAND_4G_total']; ?>)</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="morris_chart_1" style="height:500px;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-home font-green"></i>
                    <span class="caption-subject font-green bold uppercase">PCI Distribution</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="morris_chart_2" style="height:500px;"></div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-home font-green"></i>
                    <span class="caption-subject font-green bold uppercase">RSI Distribution</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="morris_chart_3" style="height:500px;"></div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-home font-green"></i>
                    <span class="caption-subject font-green bold uppercase">U Distribution</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="morris_chart_4" style="height:500px;"></div>
            </div>
        </div>
    </div>


</div>
<!-- END : MORRIS CHARTS -->

<script>
jQuery(document).ready(function() {
  // MORRIS CHARTS DEMOS

  // PIE CHART
  new Morris.Donut({
    element: 'morris_chart_1',
    data: [
      {label: "Dual band", value: <?=$distribution['BAND_4G_dual']; ?>},
      {label: "LTE1800 only", value: <?=$distribution['BAND_4G_1800']; ?>},
      {label: "LTE800 only", value: <?=$distribution['BAND_4G_800']; ?>}
    ]
  });


  // BAR CHART
  new Morris.Bar({
    element: 'morris_chart_2',
    data: [
      <?php $i = 0 ?>
        <?php foreach ($distribution_pci as $pci){ ?>
          <?php if($i<100){ ?>
      { y: <?=$pci['PCI']; ?>, a: <?=$pci['800']; ?>, b: <?=$pci['1800']; ?> },
      <?php } $i++; ?>

      <?php } ?>
    ],
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['BAND 4G 800', 'BAND 4G 1800']
  });


  // BAR CHART
  new Morris.Bar({
    element: 'morris_chart_3',
    data: [
      <?php $i = 0 ?>
        <?php foreach ($distribution_rsi as $rsi){ ?>
          <?php if($i<100){ ?>
      { y: <?=$rsi['RSI']; ?>, a: <?=$rsi['800']; ?>, b: <?=$rsi['1800']; ?> },
      <?php } $i++; ?>

      <?php } ?>
    ],
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['BAND 4G 800', 'BAND 4G 1800']
  });

  // BAR CHART
  new Morris.Bar({
    element: 'morris_chart_4',
    data: [
      <?php $i = 0 ?>
        <?php foreach ($distribution_u as $u){ ?>
          <?php if($i<100){ ?>
      { y: <?=$u['U']; ?>, a: <?=$u['800']; ?>, b: <?=$u['1800']; ?> },
      <?php } $i++; ?>

      <?php } ?>
    ],
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['BAND 4G 800', 'BAND 4G 1800']
  });

  



});
</script>
