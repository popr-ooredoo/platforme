<?php
// Note, this cannot be namespaced for the time being due to how CI works
//namespace Restserver\Libraries;

defined('BASEPATH') OR exit('No direct script access allowed');
 
require_once APPPATH."/third_party/PHPExcel/PHPExcel.php";
require_once APPPATH."/third_party/PHPExcel/PHPExcel/IOFactory.php";

class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
}